//
// Created by weber on 09.07.2017.
//

#ifndef ORAMEVALUATOR_EVALUATOR_H
#define ORAMEVALUATOR_EVALUATOR_H

#include "../oram/oram_factory.h"
#include <ctime>
#include "../fast_calculator.h"
#include "../oram/optimized_square_root.h"
#include "../types/outType.h"
#include "../types/evalParam.h"
#include "../settings.h"
#include "access_functions.h"
#include "../types/minSettings.h"

class Evaluator {
public:
    typedef outType& (*btFunc)(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t count);
    typedef outType& (*pathFunc)(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t stash, uint16_t count);
    typedef outType& (*sqrFunc)(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t T, uint16_t c, uint16_t count);

    struct funcTypes {
        btFunc btF;
        pathFunc pathF;
        pathFunc pathSCF;
        pathFunc SCORAMF;
        pathFunc CORAMF;
        sqrFunc OSQRF;
    };

    funcTypes acc_slow_func {acc_BT_slow, acc_Path_slow, acc_PathSC_slow, acc_SCORAM_slow, acc_CORAM_slow, acc_OSQR_slow};
    funcTypes acc_slow_worst_func {acc_BT_slow, acc_Path_slow, acc_PathSC_slow, acc_SCORAM_slow, acc_CORAM_slow, acc_OSQR_slow_worst};
    funcTypes slow_func {BT_slow, Path_slow, PathSC_slow, SCORAM_slow, CORAM_slow, OSQR_slow};
    funcTypes amortized_slow_func {BT_slow_amortized, Path_slow_amortized, PathSC_slow_amortized, SCORAM_slow_amortized, CORAM_slow_amortized, OSQR_slow_amortized};
    funcTypes acc_fast_func {acc_BT_fast, acc_Path_fast, acc_PathSC_fast, acc_SCORAM_fast, acc_CORAM_fast, acc_OSQR_fast};
    funcTypes fast_func {BT_fast, Path_fast, PathSC_fast, SCORAM_fast, CORAM_fast, OSQR_fast};

    minSettings find_LinearScan(uint64_t noRead, uint64_t noWrite, uint64_t m, uint64_t b);

    btSettings find_best_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, evalParam bParam, btFunc acc);
    void find_best_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, btSettings &minSettings, btFunc acc);
    void find_best_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, uint16_t c, btSettings &minSettings, btFunc acc);

    pathSettings find_best_Path(uint64_t noAcc, bool values, uint64_t m, uint64_t b, std::string type, evalParam bParam, evalParam sParam, pathFunc acc);
    pathSettings find_best_Path(uint64_t noAcc, bool values, uint64_t m, uint64_t b, std::string type, uint16_t B, uint16_t stash, pathSettings& minSettings, pathFunc acc);

    sqrtSettings find_best_OSQR(uint64_t noAcc, bool values, uint64_t m, uint64_t b, evalParam tParam, sqrFunc acc);
    sqrtSettings find_best_OSQR(uint64_t noAcc, bool values, uint64_t m, uint64_t b, evalParam tParam, sqrtSettings& min, sqrFunc acc);

    minSettings find_FLORAM(uint64_t noSecretRead, uint64_t noConstRead, uint64_t noSecretWrite, uint64_t noConstWrite, bool values, uint64_t m, uint64_t b);
    minSettings find_FLORAM_CPRG(uint64_t noSecretRead, uint64_t noConstRead, uint64_t noSecretWrite, uint64_t noConstWrite, bool values, uint64_t m, uint64_t b);

    minSettings evaluate(uint64_t noSecretRead, uint64_t noConstRead, uint64_t noSecretWrite, uint64_t noConstWrite, bool values, uint64_t m, uint64_t b);
};

#endif //ORAMEVALUATOR_EVALUATOR_H