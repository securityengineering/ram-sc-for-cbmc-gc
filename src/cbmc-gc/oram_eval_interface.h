//
// Created by anon on 19.10.17.
//

#ifndef CBMC_GC_ORAM_EVAL_INTERFACE_H
#define CBMC_GC_ORAM_EVAL_INTERFACE_H

#include "oram_goto_symext.h"
#include "bv_cbmc_ramsc.h"
#include "oram-evaluator/types/minSettings.h"
#include "oram-evaluator/evaluation/evaluator.h"
#include "oram-evaluator/evaluation/csvFileWriter.h"

inline bv_cbmc_ramsc::oram_instancet* evaluate_single(oramt* oram) {
    // get new evaluator
    Evaluator eval;
    std::cout << "\n\nPass to ORAM-Evaluator Library:\n" << *oram << std::endl;

    // set network setting
    std::cout << "\n\nset network settings to: 0.5ms, 1.3 Gbit/s, 4*10000 gates/ms" << std::endl;
    Settings::get().set(0.5, 1.3*1000, 10000*4, 80);
    minSettings settings = eval.evaluate(oram->secretRead, oram->constRead, oram->secretWrite, oram->constWrite, oram->initValues,
                                         (uint64_t) oram->noElements.to_long(), (uint64_t) oram->elementSize.to_long());
    std::cout << "leads to:\n" << settings << std::endl;
    delete settings.out;

    // set network setting
    std::cout << "\n\nset network settings to: 5ms, 1 Gbit/s, 4*10000 gates/ms" << std::endl;
    Settings::get().set(5, 1000, 10000*4, 80);
    settings = eval.evaluate(oram->secretRead, oram->constRead, oram->secretWrite, oram->constWrite, oram->initValues,
                                         (uint64_t) oram->noElements.to_long(), (uint64_t) oram->elementSize.to_long());
    std::cout << "leads to:\n" << settings << std::endl;
    delete settings.out;

    // set network setting
    std::cout << "\n\nset network settings to: 80ms, 100 Mbit/s, 4*10000 gates/ms" << std::endl;
    Settings::get().set(80, 100, 10000*4, 80);
    settings = eval.evaluate(oram->secretRead, oram->constRead, oram->secretWrite, oram->constWrite, oram->initValues,
                                         (uint64_t) oram->noElements.to_long(), (uint64_t) oram->elementSize.to_long());
    std::cout << "leads to:\n" << settings << std::endl;
    delete settings.out;

    // instantiate best ORAM type (create new oram instance)
    // TODO: set function pointers accordingly
    bv_cbmc_ramsc::oram_instancet* instance = new bv_cbmc_ramsc::oram_instancet(oram);

    return instance;
}

inline void plot_multiple(const oramMapping& oramMap) {
    Evaluator eval;
    CSVFileWriter writer("oram_plot");

    // set network setting
    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);

    outType lsRes, coramRes, osqrRes, floramRes, fcprgRes, bestRes;

    for(auto it = oramMap.begin(); it != oramMap.end(); ++it) {
        oramt* oram = (*it).second;
        uint32_t noAcc = oram->secretRead + oram->secretWrite + oram->constRead + oram->constWrite;
        lsRes += *eval.find_LinearScan(oram->secretRead, oram->secretWrite,
                                      (uint64_t)oram->noElements.to_long(),
                                      (uint64_t)oram->elementSize.to_long()).out;

        coramRes += *eval.find_best_Path(noAcc, oram->initValues,
                                       (uint64_t)oram->noElements.to_long(),
                                       (uint64_t)oram->elementSize.to_long(),
                                         "Circuit ORAM",
                                       StandardPathB(myLog2((uint64_t)oram->noElements.to_long())),
                                       StandardPathS(),
                                      eval.slow_func.CORAMF).out;
        osqrRes += *eval.find_best_OSQR(noAcc, oram->initValues,
                                        (uint64_t)oram->noElements.to_long(),
                                        (uint64_t)oram->elementSize.to_long(),
                                        StandardSqrT(myLog2((uint64_t)oram->noElements.to_long())),
                                        eval.slow_func.OSQRF).out;
        floramRes += *eval.find_FLORAM(oram->secretRead, oram->constRead, oram->secretWrite, oram->constWrite,oram->initValues,
                                  (uint64_t) oram->noElements.to_long(),
                                  (uint64_t) oram->elementSize.to_long()).out;
        fcprgRes += *eval.find_FLORAM_CPRG(oram->secretRead, oram->constRead, oram->secretWrite, oram->constWrite,oram->initValues,
                                  (uint64_t) oram->noElements.to_long(),
                                  (uint64_t) oram->elementSize.to_long()).out;
        bestRes += *eval.evaluate(oram->secretRead, oram->constRead,
                                  oram->secretWrite, oram->constWrite,
                                  oram->initValues,
                                  (uint64_t) oram->noElements.to_long(),
                                  (uint64_t) oram->elementSize.to_long()).out;                                  
    }
    // add outTypes to file
    writer.addOutType(lsRes);
    writer.addOutType(coramRes);
    writer.addOutType(osqrRes);
    writer.addOutType(floramRes);
    writer.addOutType(fcprgRes);
    writer.addOutType(bestRes);
}

#endif //CBMC_GC_ORAM_EVAL_INTERFACE_H
