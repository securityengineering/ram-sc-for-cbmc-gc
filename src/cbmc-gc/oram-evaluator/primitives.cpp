//
// library of building blocks that contains t of linear gates
// and number of bits to transfer [byte] / rounds of interactions
// values are calculated depending on entry in settings file
// Created by weber on 30.06.2017.
//

#include "primitives.h"

/**
 * costs for a 2-input linear gate
 * @return costs (gates, traffic, rounds) for one gate
 */
outType& c_lin_gate() {
    auto* out = new outType;
    *out = {1, (uint64_t) 2*Settings::get().getYaoSecurity(), 0};
    return *out;
}

/**
 * conversion of Yao to Boolean sharing is for free
 * @param m number of elements to share
 * @param b bit per element to share
 * @return costs (gates, traffic, rounds) for Y2B share conversion
 */
outType& c_Y2B(uint64_t m, uint64_t b) {
    auto* out = new outType;
    *out = {0, 0, 0};
    return *out;
}

/**
 *
 * @param m
 * @param b
 * @return
 */
outType& c_B2Y(uint64_t m, uint64_t b) {
    auto* out = new outType;
    *out = {0, 2*Settings::get().getYaoSecurity()*m*b, 2};
    return *out;
}

/**
 * costs for establishing a Yao Share
 * @param m number of elements to share
 * @param b bitwidth per element
 * @return costs /gates, traffic, rounds) for establishing a Yao share
 */
outType& c_yaoShare(uint64_t m, uint64_t b) {
    auto* out = new outType;
    *out = {0, 3*Settings::get().getYaoSecurity()*m*b, 2};
    return *out;
}

/**
 * costs for establishing a Boolean Share
 * @param m number of elements to share
 * @param b bitwidth per element
 * @return costs /gates, traffic, rounds) for establishing a Boolean share
 */
outType& c_booleanShare(uint64_t m, uint64_t b) {
    auto* out = new outType;
    *out = {0, m*b, 1};
    return *out;
}

/**
 * costs for a b-bit 2:1 multiplexer
 * @param b: number of bits to represent the two elements
 * @return costs (gates, traffic, rounds) for the multiplexer
 */
outType& c_mux(uint64_t b) {
    //assert(b != 0);       // TODO fails??
    return b*c_lin_gate();
}

/**
 * costs for a b-bit m:1 tree multiplexer
 * @param m: number of elements to connect via the multiplexer
 * @param b: number of bits of the respective elements
 * @return costs (gates, traffic, rounds) for the multiplexer tree
 */
outType& c_mux(uint64_t m, uint64_t b) {
    assert(m != 0);
    // (m-1)*c_mux(b)
    return (m-1)*c_mux(b);
}

/**
 * costs for a b-bit m+1:1 multiplexer chain of m elements and one dummy element
 * @param m: number of elements to connect excluding the dummy
 * @param b: number of bits of the respective elements
 * @return costs (gates, traffic, rounds) for the multiplexer chain
 */
outType& c_mux_chain(uint64_t m, uint64_t b) {
    assert(m != 0);
    // m*c_mux(b)
    return m*c_mux(b);
}

/**
 * costs for adding two b-bit numbers (excluding carry out)
 * @param b: number of bits of the two numbers
 * @return costs (gates, traffic, rounds) of addition
 */
outType& c_adder(uint64_t b) {
    assert(b != 0);
    return (b-1)*c_lin_gate();
}

/**
 * costs for adding two b-bit numbers (including carry out)
 * @param b: number of bits of the two numbers
 * @return costs (gates, traffic, rounds) of addition
 */
outType& c_adder_carry(uint64_t b) {
    assert(b != 0);
    return b*c_lin_gate();
}

/**
 * costs for equality comparison of two b-bit numbers
 * @param b: number of bits of the two numbers
 * @return costs (gates, traffic, rounds) of equality comparison
 */
outType& c_comp_eq(uint64_t b) {
    assert(b != 0);
    // b-1 OR
    return (b-1)*c_lin_gate();
}

/**
 * costs for magnitude comparison of two b-bit numbers
 * @param b: number of bits of the two numbers
 * @return costs (gates, traffic, rounds) of magnitude comparison
 */
outType& c_comp_mag(uint64_t b) {
    // add carry
    return c_adder_carry(b);
}

/**
 * locally (!) calculate difference between two b-bit numbers
 * @param b: number of bits of the two numbers
 * @return costs (gates, traffic, rounds) of difference calculation
 */
outType& c_calc_diff(uint64_t b) {
    return 0*c_lin_gate();
}

/**
 * costs for counting leading zeros of a b-bit number
 * @param b: number of bits to represent number
 * @return costs (gates, traffic, rounds) for counting leading zeros
 */
outType& c_LZC(uint64_t b) {
    assert(b != 0);
    return (3*b-2)*c_lin_gate();
}

/**
 * decodes a b-bit value into one-hot-code
 * @param b: number of bits to represent value
 * @return costs (gates, traffic, rounds) for decoding
 */
outType& c_decode(uint64_t b) {
    return (pow(2, b)-1)*c_lin_gate();
}

/**
 * costs for swapping two b-bit elements if the select input is true
 * @param b: number of bits of elements to swap
 * @return costs (gates, traffic, rounds) for conditionally swapping two b-bit values
 */
outType& c_condSwap(uint64_t b) {
    assert(b != 0);
    return b*c_lin_gate();
}

/**
 * costs for one comparison element, returns maximum and minimum element of two b-bit input elements
 * @param b: number of bits of elements
 * @param b2: number of bits to represent the tag to sort by
 * @return costs (gates, traffic, rounds) for one comparison element for b-bit elements with b2-bit tags
 */
outType& c_compEl(uint64_t b, uint64_t b2) {
    // CompMag(b2)+CondSwap(b)
    return c_comp_mag(b2) + c_condSwap(b);
}

/**
 * costs for sorting m b-bit elements by their b2 bit tag
 * @param m: number of elements to sort
 * @param b: number of bits of elements
 * @param b2: number of bits to represent the tag to sort by
 * @return costs (gates, traffic, rounds) fort sorting
 */
outType& c_sort(uint64_t m, uint64_t b, uint16_t b2) {
    // compEl(b, b2)*((1/4)*m*(log^2(m)-log(m))+m-1)
    auto rounded = (uint64_t) ceil((double) m/4);
    auto powed = (uint64_t) pow(myLog2(m),2);
    return (rounded*(powed-myLog2(m))+m-1)*c_compEl(b, b2);
}

/**
 * costs for executing a waksman network, excluding costs for transmission of control bits
 * @param m
 * @param b
 * @return
 */
outType& c_waksman(uint64_t m, uint64_t b) {
    // condSwap(b) * (m * log2(m) - m + 1)
    uint64_t elements = m * myLog2(m) - m + 1;
    return elements*c_condSwap(b);
}

/**
 * TODO: überarbeiten, das ist total komisch so
 * costs for shuffling m b-bit elements using given control bits
 * @param m: number of elements to shuffle
 * @param b: number of bits of elements
 * @return costs (gates, traffic, rounds) to shuffle the elements
 */
outType& c_shuffle(uint64_t m, uint64_t b, bool generator) {
    if(generator) {
        // generator party has control bits -> gates can be pre-evaluated
        // send half-gates of circuit to evaluator (should be possible to transmit in parallel with other gates)
        return c_waksman(m, b) / 2;
    }
    else {
        // evaluator party has control bits -> no circuit necessary
        // 1. convert control bits to Yao Share
        // 2. send garbled inputs of generator (should be possible to do in parallel)
        return c_yaoShare(1, m*myLog2(m)-m+1) + c_transmit(Settings::get().getYaoSecurity()*(m*myLog2(m)-m+1));
    }
}

/**
 * costs for comparing each element to its neighbours to determine duplicates
 * @param m: number of elements to filter
 * @param b: number of bit to compare by
 * @return costs (gates, traffic, rounds) for filtering duplicates
 */
outType& c_dDup(const uint64_t m, const uint64_t b) {
    assert(m > 1);
    // (m-1)*compEq(b)
    return (m-1)*c_comp_eq(b);
}

/**
 * pseudo random generator (AES)
 * @return
 */
outType& c_SC_PRG() {
    return (2*5120)*c_lin_gate();
}

/**
 * pseudo random generator (AES)
 * @return
 */
outType& c_L_PRG() {
    auto* out = new outType;
    *out = {0, 0, 0};
    std::cout << "WARNING: c_L_PRG is not implemented yet!!" << std::endl;
    return *out;
}

/**
 * pseudo random function
 * @param m: number of blocks to encrypt
 * @return
 */
outType& c_SC_PRF(uint64_t m) {
    // 1. get Nonce // TODO: number of bits???
    // 2. use index as counter value -> for free
    // 3. combine Nonce and index       // TODO: wie? steht nirgendwo
    // 4. encrypt (nonce | counter) with AES
    // 5. combine result with plaintext block -> XOR -> for free
    return m*c_SC_PRG();
}

/**
 * pseudo random function
 * @return
 */
outType& c_L_PRF(uint64_t m) {
    auto* out = new outType;
    *out = {0, 0, 0};
    std::cout << "WARNING: c_L_PRF is not implemented yet!!" << std::endl;
    return *out;
}

/**
 * costs for returning an arbitrary b-bit value
 * @param b number of bit for random value
 * @return costs (gates, traffic, rounds) for creating an arbitrary b-bit value
 */
outType& c_rand(uint64_t b) {
    auto* out = new outType;
    *out = {0, 2*b, 2};
    return *out;
}

outType& c_transmit(uint64_t b) {
    auto* out = new outType;
    *out = {0, b, 1};
    return *out;
}

outType& c_transmit_in_same_round(uint64_t b) {
    auto* out = new outType;
    *out = {0, b, 0};
    return *out;
}
