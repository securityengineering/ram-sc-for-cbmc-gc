//
// Created by weber on 20.08.2017.
//

#ifndef ORAMEVALUATOR_OPTIMIZED_SQUARE_ROOT_H
#define ORAMEVALUATOR_OPTIMIZED_SQUARE_ROOT_H

#include "oram.h"
#include "linear_scan_oram.h"
#include "map_based_oram.h"

class OSquareRoot : public MapBasedORAM {
protected:
    uint16_t T;                     // period
    uint16_t t;                     // number accesses this period
    bool isMap;
public:
    OSquareRoot(uint64_t m, uint64_t b, uint16_t T, uint16_t c) : ORAM(m, b, b + myLog2(m), "Optimized Square-Root"),
                                                                  MapBasedORAM(c), T(T), t(0), isMap(false) { }

    OSquareRoot(uint64_t m, uint64_t b, uint16_t T, uint16_t c, bool isMap) :
            ORAM(m, b, b + myLog2(m), "Optimized Square-Root"), MapBasedORAM(c), T(T), t(0), isMap(isMap) { }

    inline TrivialLinearScan* createLSMap(uint64_t newM) override {
        return new SQR_TLS(m, (uint64_t) myLog2(m));
    }

    inline OSquareRoot* createMap(uint64_t newM) override {
        return new OSquareRoot(newM, c*myLog2(m), T, c, true);
    }

    inline bool recursionCond(uint16_t counter) override {
        return counter > 0 && m >= c*T;
    }
    outType& c_LUMU();

    outType& c_init(bool values) override;
    outType& c_acc(uint64_t b) override;
    outType& c_acc(uint64_t noAcc, uint64_t b);
    outType& c_acc_worst(uint64_t b);
    outType& c_amortized(uint64_t noAcc, bool values) override;

    inline uint16_t getT() { return T; }

private:
    outType& c_scan_stash();
    outType& c_refresh();
    outType& c_update();
    outType& c_permute();
};


class OSquareRootOrig : public OSquareRoot {
public:
    OSquareRootOrig(uint64_t m, uint64_t b, uint16_t T, uint16_t c) : OSquareRoot(m, b, T, c) { }

    OSquareRootOrig(uint64_t m, uint64_t b, uint16_t T, uint16_t c, bool isMap) : OSquareRoot(m, b, T, c, isMap) { }

    inline bool recursionCond(uint16_t counter) override {
        return (m+c-1)/c >= T;
    }

    inline OSquareRoot* createMap(uint64_t newM) override {
        return new OSquareRootOrig(newM, c*myLog2(m), T, c, true);
    }
};

#endif //ORAMEVALUATOR_OPTIMIZED_SQUARE_ROOT_H