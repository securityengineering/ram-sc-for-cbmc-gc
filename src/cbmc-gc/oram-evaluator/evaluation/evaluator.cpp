//
// Created by weber on 09.07.2017.
//

#include "evaluator.h"

minSettings Evaluator::find_LinearScan(uint64_t noRead, uint64_t noWrite, uint64_t m, uint64_t b) {
    outType& out = addWR(multiplyWR(noRead, TrivialLinearScan::c_read(m, b)),
                         multiplyWR(noWrite, TrivialLinearScan::c_write(m, b)));
    return minSettings("Trivial Linear Scan", LS_ID, 0, 0, &out);
}


btSettings Evaluator::find_best_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, evalParam bParam,
                                               btFunc acc) {
    btSettings minSettings;

    for (uint16_t B = bParam.min; B <= bParam.max; B = B * bParam.step_m + bParam.step_p)
        find_best_BT(noAcc, values, m, b, B, minSettings, acc);

    return minSettings;
}

void Evaluator::find_best_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, btSettings& minSettings,
                                              btFunc acc) {
    for (uint16_t c = 2; c <= myLog2(m); c *= 2)
        find_best_BT(noAcc, values, m, b, B, c, minSettings, acc);
}

void Evaluator::find_best_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, uint16_t  c, btSettings& minSettings,
                             btFunc acc) {
    for (uint16_t count = 1; count <= floor(log(m) / log(c)); count++) {
        outType &out = multiplyWR(noAcc, acc(noAcc, values, m, b, B, c, count));
        if (out < *minSettings.out) {
            *minSettings.out = {out.gates, out.traffic, out.rounds};
            minSettings = {"Binary Tree ORAM", B, c, count, minSettings.out};
        }
        delete &out;
    }
}

pathSettings Evaluator::find_best_Path(uint64_t noAcc, bool values, uint64_t m, uint64_t b, std::string type,
                                       evalParam bParam, evalParam sParam, pathFunc acc) {
    pathSettings minSettings;
    for(uint16_t stash = sParam.min; stash <= sParam.max; stash = stash*sParam.step_m + sParam.step_p)
        for(uint16_t B = bParam.min; B <= bParam.max; B = B*bParam.step_m + bParam.step_p)
            find_best_Path(noAcc, values, m, b, type, B, stash, minSettings, acc);

    std::cout << "best path: B: " << minSettings.B << " s: " << minSettings.stash << " c: " << minSettings.c << " count: " << minSettings.count << " out: " << minSettings.out << std::endl;
    return minSettings;
}

pathSettings Evaluator::find_best_Path(uint64_t noAcc, bool values, uint64_t m, uint64_t b, std::string type, uint16_t B,
                                                  uint16_t stash, pathSettings& minSettings, pathFunc acc) {
    uint16_t d = myLog2(m);

    for (uint16_t c = 2; c <= (m >> 2) && c < (UINT16_MAX >> 1); c *= 2) {
        for (uint16_t count = 1; count <= floor(log(m)/log(c)); count++) {
            outType& out = 1*acc(noAcc, values, m, b, B, c, stash, count);
            if (out < *minSettings.out) {
                *minSettings.out = {out.gates, out.traffic, out.rounds};
                minSettings = {type, B, c, count, stash, minSettings.out};
            }
            delete &out;
        }
    }
    return minSettings;
}

sqrtSettings Evaluator::find_best_OSQR(uint64_t noAcc, bool values, uint64_t m, uint64_t b, evalParam tParam, sqrtSettings& min, sqrFunc acc) {

#ifdef SQRT_ORAM_OPTIMIZE_T
    for(uint16_t t = tParam.min; t <= tParam.max; t = t*tParam.step_m + tParam.step_p) {
#else     
    auto t = (uint16_t)ceil(sqrt(m * myLog2(m) - m + 1));
#endif    
        for (uint16_t c = 2; c <= (m >> 2) && c < (UINT16_MAX >> 1); c *= 2) {
            for (uint16_t count = 1; count <= floor(log(m) / log(c)); count++) {
                outType &out = 1*acc(noAcc, values, m, b, t, c, count);
                if (out < *min.out) {
                    *min.out = {out.gates, out.traffic, out.rounds};
                    min = {"Optimized Square Root ORAM", t, c, count, min.out};
                }
                delete &out;
            }
        }
#ifdef SQRT_ORAM_OPTIMIZE_T        
    }
#endif    
    std::cout << "best sqrt: T: " << min.T << " c: " << min.c << " count: " << min.count << " out: " << min.out << std::endl;
    return min;
}

sqrtSettings Evaluator::find_best_OSQR(uint64_t noAcc, bool values, uint64_t m, uint64_t b, evalParam tParam, sqrFunc acc) {
    sqrtSettings min;
    find_best_OSQR(noAcc, values, m, b, tParam, min, acc);
    return min;
}

minSettings Evaluator::find_FLORAM(uint64_t noSecretRead, uint64_t noConstRead, uint64_t noSecretWrite, uint64_t noConstWrite, bool values, uint64_t m, uint64_t b) {
    // TODO: at the moment this is worst case - do calculate avg case!
    FloramOrig* oram = ORAMFactory::create_FLORAM(m, b, UINT16_MAX);
    outType& sWrite = oram->c_acc(noSecretWrite, b);
    oram->setSteps(oram->getT() / 2);
    outType& sRead = multiplyWR(noSecretRead, oram->c_read());
    outType& cRead = multiplyWR(noConstRead, oram->c_read_static());
    outType& cWrite = multiplyWR(noConstWrite, oram->c_write_static());
    outType& out = addWR(oram->c_init(values), addWR(addWR(sWrite, sRead), addWR(cRead, cWrite)));
    delete oram;
    return minSettings("FLORAM", FLORAM_ID, 0, 0, &out);
}

minSettings Evaluator::find_FLORAM_CPRG(uint64_t noSecretRead, uint64_t noConstRead, uint64_t noSecretWrite, uint64_t noConstWrite, bool values, uint64_t m, uint64_t b) {
    // TODO: at the moment this is worst case - do calculate avg case!
    FloramOrig* oram = ORAMFactory::create_FLORAM_CPRG(m, b, UINT16_MAX);
    outType& sWrite = oram->c_acc(noSecretWrite, b);
    oram->setSteps(oram->getT() / 2);
    outType& sRead = multiplyWR(noSecretRead, oram->c_read());
    outType& cRead = multiplyWR(noConstRead, oram->c_read_static());
    outType& cWrite = multiplyWR(noConstWrite, oram->c_write_static());
    outType& out = addWR(oram->c_init(values), addWR(addWR(sWrite, sRead), addWR(cRead, cWrite)));
    delete oram;
    return minSettings("FLORAM CPRG", CPRG_ID, 0, 0, &out);
}

minSettings Evaluator::evaluate(uint64_t noSecretRead, uint64_t noConstRead, uint64_t noSecretWrite, uint64_t noConstWrite,
                         bool values, uint64_t m, uint64_t b) {
    uint64_t noAcc = noSecretRead + noConstRead + noSecretWrite + noConstWrite;
    std::cout << "evaluate: m: " << m << " b: " << b << " values? " << values << " noSecretRead: " << noSecretRead << " noSecretWrite: " << noSecretWrite << " noConstRead: " << noConstRead << " noConstWrite: " << noConstWrite << std::endl;

    funcTypes func = slow_func;             // TODO
    minSettings settings[5];

    settings[0] = find_LinearScan(noSecretRead, noSecretWrite, m, b);
    std::cout << "\nTrivial Linear Scan result: " << *(settings[0]).out << std::endl;

    settings[1] = find_best_Path(noAcc, values, m, b, "Circuit ORAM", StandardPathB(myLog2(m)), StandardPathS(), func.CORAMF);
    std::cout << "Circuit ORAM result: " << *(settings[1]).out << std::endl;

    settings[2] = find_best_OSQR(noAcc, values, m, b, StandardSqrT(myLog2(m)), func.OSQRF);
    std::cout << "Optimized Square Root ORAM result: " << *(settings[2]).out << "\n" << std::endl;

    settings[3] = find_FLORAM(noSecretRead, noConstRead, noSecretWrite, noConstWrite, values, m, b);
    std::cout << "FLORAM result: " << *(settings[3]).out << "\n" << std::endl;

    settings[4] = find_FLORAM_CPRG(noSecretRead, noConstRead, noSecretWrite, noConstWrite, values, m, b);
    std::cout << "FLORAM CPRG result: " << *(settings[4]).out << "\n" << std::endl;

    minSettings& min = settings[0];
    for(uint8_t i = 1; i <= 4; i++) {
        if(*settings[i].out < *min.out) {
            delete min.out;
            min = settings[i];
        }
        else delete settings[i].out;
    }
    std::cout << "best result: " << min << "\n-----------\n" << std::endl;
    return min;
}
