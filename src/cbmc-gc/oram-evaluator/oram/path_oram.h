//
// Created by weber on 30.06.2017.
//

#ifndef ORAMEVALUATOR_PATH_ORAM_H
#define ORAMEVALUATOR_PATH_ORAM_H

#include "binary_tree_oram.h"
#include "../helper.h"
#include "../types/minSettings.h"
#include <cassert>
#include <utility>
#include <unordered_map>

class Path : public TreeInterface {
protected:
    uint16_t s;
    LinearScan* stash{};
    uint64_t accCount = 0;
public:
    Path(uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t s) : ORAM(m, b, b+2*myLog2(m), "Path ORAM"),
                                                                       TreeInterface((uint16_t) (myLog2(m)-1), B, c),
                                                                       s(s) { }
    Path(uint64_t m, uint64_t b, uint16_t d, uint16_t B, uint16_t c, uint16_t s) : ORAM(m, b, b+myLog2(m)+1+d, "Path ORAM"),
                                                                       TreeInterface(d, B, c),
                                                                       s(s) { }

    ~Path() override {
        delete stash;
    }

    void build(uint16_t counter) override;

    ORAM* createMap(uint64_t newM) override;

    inline bool recursionCond(uint16_t counter) override {
        return counter > 0 && m > 8*c;          // d = log(m)-1 and log(d) is used
    }

    LinearScanOram* createBuckets() override;

    outType& c_init(bool values) override;
    outType& c_acc(uint64_t b) override;
    outType& c_RAR(uint64_t b) override;

    outType& c_LCA(uint64_t b);
    virtual outType& c_addAndEvict();
};

class PathSC : public Path {
public:
    PathSC(uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t s) : ORAM(m, b, b+2*myLog2(m), "PathSC ORAM"),
                                                                         Path(m, b, B, c, s) {
        // B has to be power of two otherwise c_offset would need to include multiplier
        assert(isPowerOfTwo(B));
    }
    PathSC* createMap(uint64_t newM) override;
    outType& c_addAndEvict() override;

    outType& c_dud(uint64_t m, uint64_t b);
    outType& c_offset(uint64_t m, uint64_t b);
    outType& sort1();
    outType& sort23();
};

class Scoram : public Path {
protected:
    uint8_t alpha;
public:
    Scoram(uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t s, uint8_t alpha) : ORAM(m, b, b+2*myLog2(m), "SCORAM"),
                                                                                        Path(m, b, B, c, s), alpha(alpha) { }
    Scoram* createMap(uint64_t newM) override;

    outType& c_acc(uint64_t b) override;
    outType& c_RAR(uint64_t b) override;
    outType& c_addAndEvict() override;

    outType& c_cPut();
    outType& c_minLCA(uint64_t m, uint64_t b);
    outType& c_RDP();
    outType& c_GPP();
};

class Coram : public Path {
public:
    Coram(uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t s) : ORAM(m, b, b+2*myLog2(m)+1, "Circuit ORAM"),
                                                                        Path(m, b, myLog2(m), B, c, s) { }
    Coram(uint64_t m, uint64_t b, uint16_t d, uint16_t B, uint16_t c, uint16_t s) : ORAM(m, b, b+myLog2(m)+1+d, "Circuit ORAM"),
                                                                        Path(m, b, d, B, c, s) { }
    void build(uint16_t counter) override;

    ORAM* createMap(uint64_t newM) override;
    outType& c_addAndEvict() override;
    LinearScanOram* createBuckets() override;

    TrivialLinearScan* createLSMap(uint64_t newM) override {
        //std::cout << "TLS: m: " << m << " b: " << myLog2(m) << std::endl;
        return new TrivialLinearScan(m, myLog2(m));
    }

    outType& c_LUMU() override;

    outType& c_RAR(uint64_t b) override;

    outType& c_minLCA(uint64_t m, uint64_t b);
    outType& c_PDStash();
    outType& c_PDStage();
    outType& c_PD();
    outType& c_PT();
    outType& c_evictOnceW(bool evictStash);
    outType& c_evictOnce();
};

// only used for verification of SQR paper
class SQR_CORAM: public Coram {
public:
    SQR_CORAM(uint64_t m, uint64_t b, uint16_t B, uint16_t c, uint16_t s) : ORAM(m, b, b+2*myLog2(m)+1, "Circuit ORAM"),
                                                                            Coram(m, b, B, c, s) {
        //std::cout << "CORAM: m: " << m << " b: " << b << std::endl;
    }

    inline bool recursionCond(uint16_t counter) override {
        return counter > 0 && m > pow(2, 8);
    }

    inline ORAM* createMap(uint64_t newM) override {
        return new SQR_CORAM(newM, c*d, B, c, s);
    }

    outType& c_write(uint64_t b);
};
#endif //ORAMEVALUATOR_PATH_ORAM_H