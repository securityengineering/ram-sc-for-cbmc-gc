//
// Created by anon on 02.09.17.
//

#include "anon_output.h"

// #define PRINT_STEP_1
// #define PRINT_STEP_2
// #define PRINT_STEP_3
// #define PRINT_STEP_4
// #define PRINT_STEP_5
// #define PRINT_STEP_6
// #define PRINT_STEP_7
// #define PRINT_EXPRESSIONS
 #define ONLY_RELEVANT
// #define PRINT_MISC

// #define PRINT_TO_INDEX
// #define PRINT_TO_ARRAY
// #define PRINT_TO_MEMBER
// #define PRINT_TO_CONSTANT

/*
 * important note:
 * trigger fpr symex steps is in symex_main.cpp -> goto_symext::symex_step
 * trigger for full printing of currently handled assignment expression is in symex_target_equationt::convert_assignments
 */


bool filter_expr(const exprt& e) {
    for(exprt::operandst::const_iterator it = e.operands().begin(); it != e.operands().end(); it++)
        if(it.operator*().find_source_location().get_file() == "<built-in-additions>")
            return false;
    return e.find_source_location().get_file() != "<built-in-additions>"
           && e.find_source_location().get_file() != ""
           && e.find_source_location().get_file() != "gcc_builtin_headers_generic.h"
           && e.find_source_location().get_file() != "clang_builtin_headers.h";
}
//

void print_always(const std::string& s) {
    std::cout << "\n" << s << "\n" << std::endl;
}

void print_always_expr(const std::string& s, const exprt& e, bool print_fully) {
/*#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_always(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_always(s + ": " + e.id_string());*/
}

void print_always(const std::string& s, const exprt& e, bool print_fully) {
/*#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_always_expr(s, e, print_fully);
    else if(print_fully)
        print_always_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_always_expr(s, e, print_fully);*/
}

void print_to_index_expr(const exprt& e) {
#if defined(PRINT_TO_INDEX) && defined(ONLY_RELEVANT)
    if(filter_expr(e))
        std::cout << "to_index_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#if defined(PRINT_TO_INDEX) && !defined(ONLY_RELEVANT)
    std::cout << "to_index_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#ifndef ONLY_RELEVANT
    std::cout << "to_index_expr: " << e.id_string() << "\n" << std::endl;
#endif
}

void print_to_array_expr(const exprt& e) {
#if defined(PRINT_TO_ARRAY) && defined(ONLY_RELEVANT)
    if(filter_expr(e))
        std::cout << "to_array_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#if defined(PRINT_TO_ARRAY) && !defined(ONLY_RELEVANT)
    std::cout << "to_array_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#ifndef ONLY_RELEVANT
    std::cout << "to_array_expr: " << e.id_string() << "\n" << std::endl;
#endif
}

void print_to_member_expr(const exprt& e) {
#if defined(PRINT_TO_MEMBER) && defined(ONLY_RELEVANT)
    if(filter_expr(e))
        std::cout << "to_member_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#if defined(PRINT_TO_MEMBER) && !defined(ONLY_RELEVANT)
    std::cout << "to_member_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#ifndef ONLY_RELEVANT
    std::cout << "to_member_expr: " << e.id_string() << "\n" << std::endl;
#endif
}

void print_to_constant_expr(const exprt& e) {
#if defined(PRINT_TO_CONSTANT) && defined(ONLY_RELEVANT)
    if(filter_expr(e))
        std::cout << "to_constant_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#if defined(PRINT_TO_CONSTANT) && !defined(ONLY_RELEVANT)
    std::cout << "to_constant_expr: " << e.pretty() << "\n" << std::endl;
    return;
#endif
#ifndef ONLY_RELEVANT
    std::cout << "to_constantx_expr: " << e.id_string() << "\n" << std::endl;
#endif
}

void print_misc(const std::string& s) {
#ifdef PRINT_MISC
    std::cout << s << std::endl;
#endif
}

void print_misc_expr(const std::string& s, const exprt& e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_misc(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_misc(s + ": " + e.id_string());
}

void print_misc(const std::string& s, const exprt& e, bool print_fully) {
#ifdef ONLY_RELEVANT
    //std::cout << "\nlocation: " << e.find_source_location().get_file() << std::endl;
    if(filter_expr(e))
        print_misc_expr(s, e, print_fully);
    else if(print_fully)
        print_misc_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_misc_expr(s, e, print_fully);
}

void print_step_1(const std::string& s) {
#ifdef PRINT_STEP_1
    std::cout << s << std::endl;
#endif
}

void print_step_1_expr(const std::string& s, const exprt& e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_1(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_1(s + ": " + e.id_string());
}

void print_step_1(const std::string& s, const exprt& e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_1_expr(s, e, print_fully);
    else if(print_fully)
        print_step_1_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_1_expr(s, e, print_fully);
}

void print_step_2(const std::string &s) {
#ifdef PRINT_STEP_2
    std::cout << s << std::endl;
#endif
}

void print_step_2_expr(const std::string &s, const exprt &e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_2(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_2(s + ": " + e.id_string());
}

void print_step_2(const std::string &s, const exprt &e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_2_expr(s, e, print_fully);
    else if(print_fully)
        print_step_2_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_2_expr(s, e, print_fully);
}

void print_step_3(const std::string &s) {
#ifdef PRINT_STEP_3
    std::cout << s << std::endl;
#endif
}

void print_step_3_expr(const std::string &s, const exprt &e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_3(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_3(s + ": " + e.id_string());
}

void print_step_3(const std::string &s, const exprt &e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_3_expr(s, e, print_fully);
    else if(print_fully)
        print_step_3_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_3_expr(s, e, print_fully);
}

void print_step_4(const std::string &s) {
#ifdef PRINT_STEP_4
    std::cout << s << std::endl;
#endif
}

void print_step_4_expr(const std::string &s, const exprt &e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_4(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_4(s + ": " + e.id_string());
}

void print_step_4(const std::string &s, const exprt &e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_4_expr(s, e, print_fully);
    else if(print_fully)
        print_step_4_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_4_expr(s, e, print_fully);
}

void print_step_5(const std::string &s) {
#ifdef PRINT_STEP_5
    std::cout << s << std::endl;
#endif
}

void print_step_5_expr(const std::string &s, const exprt &e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_5(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_5(s + ": " + e.id_string());
}

void print_step_5(const std::string &s, const exprt &e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_5_expr(s, e, print_fully);
    else if(print_fully)
        print_step_5_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_5_expr(s, e, print_fully);
}

void print_step_6(const std::string &s) {
#ifdef PRINT_STEP_6
    std::cout << s << std::endl;
#endif
}

void print_step_6_expr(const std::string &s, const exprt &e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_6(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_6(s + ": " + e.id_string());
}

void print_step_6(const std::string &s, const exprt &e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_6_expr(s, e, print_fully);
    else if(print_fully)
        print_step_6_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_6_expr(s, e, print_fully);
}

void print_step_7(const std::string &s) {
#ifdef PRINT_STEP_7
    std::cout << s << std::endl;
#endif
}

void print_step_7_expr(const std::string &s, const exprt &e, bool print_fully) {
#ifdef PRINT_EXPRESSIONS
    if(print_fully) {
        print_step_7(s + ": " + e.pretty() + "\n");
        return;
    }
#endif
    print_step_7(s + ": " + e.id_string());
}

void print_step_7(const std::string &s, const exprt &e, bool print_fully) {
#ifdef ONLY_RELEVANT
    if(filter_expr(e))
        print_step_7_expr(s, e, print_fully);
    else if(print_fully)
        print_step_7_expr(s + " - filtered: location: " + e.find_source_location().as_string() + " type", e, false);
    return;
#endif
    print_step_7_expr(s, e, print_fully);
}