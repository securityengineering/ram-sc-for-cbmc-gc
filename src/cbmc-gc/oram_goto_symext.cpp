//
// Created by anon on 17.09.17.
//

#include <util/arith_tools.h>
#include "oram_goto_symext.h"

std::string oram_goto_symext::filter_identifier(irep_idt identifier) {
    size_t found = std::min(id2string(identifier).find('!'), id2string(identifier).find("_l"));

    if(found != std::string::npos)
        return id2string(identifier).substr(0, found);

    return id2string(identifier);
}

std::string oram_goto_symext::get_identifier_string(irep_idt identifier, uint8_t array_rec_level) {
    return filter_identifier(identifier) + "_l" + std::to_string(array_rec_level);
}


irep_idt oram_goto_symext::handle_declaration(const array_typet& array, irep_idt identifier) {
    return handle_declaration(array, get_identifier_string(identifier, array_rec_level));
}

irep_idt oram_goto_symext::handle_declaration(const array_typet& array, std::string identifier) {
    oramMapping::iterator it = oramMap.find(identifier);
    if(it != oramMap.end())         // array is already in the map
        return irep_idt(identifier);

    mp_integer arraySize;
    assert(!to_integer(array.size(), arraySize));
    mp_integer elementSize;

    if(array.subtype().id() == ID_signedbv)
        elementSize = to_signedbv_type(array.subtype()).get_unsigned_int("width");

    else {
        // multidimensional array
        assert(array.subtype().id() == ID_array);
        array_rec_level++;
        irep_idt identifier_inner = handle_declaration(to_array_type(array.subtype()), irep_idt(identifier));

        // get inner oram
        oramMapping::iterator it_inner = oramMap.find(identifier_inner.c_str());
        assert(it_inner != oramMap.end());

        elementSize = (*it_inner).second->noElements * (*it_inner).second->elementSize;
        array_rec_level--;
    }
    oramt* oram = new oramt(identifier, arraySize, elementSize);
    oramMap[identifier] = oram;

    return identifier;
}

irep_idt oram_goto_symext::handle_member_declaration(member_exprt& member) {
    // get identifier
    irep_idt identifier = to_symbol_expr(member.op0()).get_identifier();
    oramMapping::iterator it = oramMap.find(get_identifier_string(identifier, array_rec_level));

    if(it == oramMap.end()) {
        // array isn't already in the map
        handle_declaration(to_array_type(member.type()), identifier);
        handle_member_initialization(member, get_identifier_string(identifier, array_rec_level));
    }

    return irep_idt(get_identifier_string(identifier, array_rec_level));
}

void oram_goto_symext::symex_decl(goto_symext::statet& state, const symbol_exprt& expr) {
    // array declaration without initialization (was splitted in goto phase)
    if(expr.type().id() == ID_array)
        handle_declaration(to_array_type(expr.type()), expr.get_identifier());

    goto_symext::symex_decl(state, expr);
}

void oram_goto_symext::handle_initialization(const symbol_exprt& symbol, const array_exprt& array) {
    // get identifier and initialize
    handle_initialization(array, get_identifier_string(symbol.get_identifier(), array_rec_level));
}

void oram_goto_symext::handle_initialization(const array_exprt& array, std::string identifier) {
    // array has to be already in the map
    oramMapping::iterator it = oramMap.find(identifier);
    assert(it != oramMap.end());

    // multidim array?
    if(array.op0().id() == ID_array) {
        array_rec_level++;
        std::string identifier_inner = get_identifier_string(identifier, array_rec_level);
        handle_initialization(to_array_expr(array.op0()), identifier_inner);

        oramMapping::iterator it_inner = oramMap.find(identifier_inner);
        assert(it != oramMap.end());

        // if inner arrays are initialized, the outer one is, too
        (*it).second->initValues = (*it_inner).second->initValues;
        array_rec_level--;
    }
    else (*it).second->initValues = !array.operands().empty() && array.op0().id() == ID_typecast;
}

void oram_goto_symext::handle_member_initialization(exprt& expr, std::string identifier) {
    // check if multidimensional
    if(expr.has_operands() && expr.type().id() == ID_array && to_array_type(expr.type()).subtype().id() == ID_array) {
        array_rec_level++;
        handle_member_initialization(expr.type().subtype(), get_identifier_string(identifier, array_rec_level));
        array_rec_level--;
    }

    // set initValues
    oramMapping::iterator it = oramMap.find(identifier);
    size_t found = identifier.find("INPUT");
    (*it).second->initValues = (found != std::string::npos);
}

void oram_goto_symext::handle_member_initialization(typet& type, std::string identifier) {
    // check if multidimensional
    if(type.has_subtype() && type.subtype().id() == ID_array)
        handle_member_initialization(type.subtype(), identifier);

    // set initValues
    oramMapping::iterator it = oramMap.find(identifier);
    size_t found = identifier.find("INPUT");
    (*it).second->initValues = (found != std::string::npos);
}

void oram_goto_symext::symex_assign_rec(goto_symext::statet& state, const code_assignt &code) {
    // check if this could be an array initialization
    if(code.operands().size() > 1 && code.op0().id() == ID_symbol && code.op1().id() == ID_array)
        handle_initialization(to_symbol_expr(code.op0()), to_array_expr(code.op1()));

    goto_symext::symex_assign_rec(state, code);
}

void oram_goto_symext::handle_access(exprt& expr, exprt& index, goto_symext::statet& state, irep_idt identifier, bool write) {
    // assert that array is already in the map
    oramMapping::iterator it = oramMap.find(get_identifier_string(identifier, array_rec_level));
    assert(it != oramMap.end());

    // check if index is constant (also used in bv_cbmbc::convert_index)
    mp_integer index_value;
    if (!to_integer(index, index_value))
        write? (*it).second->constWrite++ : (*it).second->constRead++;

    else write? (*it).second->secretWrite++ : (*it).second->secretRead++;
}

index_exprt& oram_goto_symext::get_innermost_index(index_exprt& index) {

    // check if this is the outer part of a multidimensional array
    if(index.op0().id() == ID_index && index.op0().type().id() == ID_array) {
        // we want to do the inner-most access first, so set the recursion level accordingly
        array_rec_level++;
        return get_innermost_index(to_index_expr(index.op0()));
    }
    return index;
}

void oram_goto_symext::handle_index(index_exprt& index, goto_symext::statet& state, bool write) {
    irep_idt identifier;

    // get the innermost index expression first
    index_exprt* index_to_use = &get_innermost_index(index);

    // check if this is a one-dimensional array defined as struct
    if(index_to_use->op0().id() == ID_member) {
        // declaration handles the recursion level for itself, so save this one temporarily
        uint8_t rec_level_temp = array_rec_level;
        array_rec_level = 0;
        identifier = handle_member_declaration(to_member_expr(index_to_use->op0()));
        array_rec_level = rec_level_temp;
    }
    else {
        // the array has to be declared before
        assert(index_to_use->op0().type().id() == ID_array);
        identifier = index_to_use->op0().type().get("array_ident");
    }
    handle_access(*index_to_use, index.op1(), state, identifier, write);

    // if this is a multidimensional write, we also have to do a read access to the outer level
    if(write && array_rec_level > 0) {
        array_rec_level--;
        index_exprt* temp = &index;

        for(int i = array_rec_level; i >= 0; i--)
            temp = &to_index_expr(temp->op0());

        handle_access(index, temp->op1(), state, index_to_use->op0().type().get("array_ident"), false);
    }

    // reset recursion level
    array_rec_level = 0;
}

std::string oram_goto_symext::complete_member_expression(member_exprt& member) {
    assert(member.op0().id() == ID_symbol);
    std::string identifier = filter_identifier(to_symbol_expr(member.op0()).get_identifier());

    // set identifier for array type of member
    member.type().add("array_ident", irept(identifier));

    if(member.op0().type().id() == ID_struct) {
        struct_typet& struct_type = to_struct_type(member.op0().type());
        // TODO: extend me!
        for(struct_union_typet::componentst::iterator it = struct_type.components().begin(); it != struct_type.components().end(); it++) {
            if((*it).type().id() == ID_array)
                (*it).type().add("array_ident", irept(identifier));
        }
    }
    return identifier;
}

std::string oram_goto_symext::complete_symbol_expression(symbol_exprt& symbol) {
    assert(symbol.id() == ID_symbol && symbol.type().id() == ID_array);
    std::string identifier = filter_identifier(symbol.get_identifier());

    // set identifier for array type of symbol
    symbol.type().add("array_ident", irept(identifier));
    return identifier;
}

void oram_goto_symext::complete_array_expression(array_exprt &array, std::string identifier) {
    assert(array.type().id() == ID_array);
    array.type().add("array_ident", irept(identifier));

    for(exprt::operandst::iterator it = array.operands().begin(); it != array.operands().end(); it++)
        if((*it).type().id() == ID_array)
            complete_array_expression(to_array_expr(*it), identifier);
}

std::string oram_goto_symext::complete_expressions(exprt& expr) {
    std::string identifier;

    if(expr.id() == ID_array && expr.has_operands()) {
        assert(expr.type().id() == ID_array);
        identifier = filter_identifier(expr.type().get("array_ident"));
        complete_array_expression(to_array_expr(expr), identifier);
    }

    else if(expr.id() == ID_member)
        identifier = complete_member_expression(to_member_expr(expr));

    else if(expr.id() == ID_index) {
        if(expr.op0().id() == ID_symbol && expr.op0().type().id() == ID_array)
            identifier = complete_symbol_expression(to_symbol_expr(expr.op0()));

        else if(expr.op0().id() == ID_member)
            identifier = complete_member_expression(to_member_expr(expr.op0()));

        else if(expr.op0().id() == ID_index)
            identifier = complete_expressions(expr.op0());

        if(expr.type().id() == ID_array)
            expr.type().add("array_ident", irept(identifier));
    }
    return identifier;
}

bool oram_goto_symext::filter_index_op(goto_symext::statet& state) {
    // GOTOs with guard == FALSE are never executed -> filter those operations
    return state.source.pc->type == GOTO &&
            state.guard.id() == ID_constant && to_constant_expr(state.guard).get_value() == ID_false;
}

void oram_goto_symext::dereference_rec(exprt& expr, goto_symext::statet& state, guardt& guard, const bool write) {
   // we need additional array identifiers, so that we can identify the array later on
   complete_expressions(expr);

   if(expr.id() == ID_index && !filter_index_op(state)) {
       // check if this is !really! an index expression or if we already know the element (also used in goto_symext::symex_assign_symbol)
       exprt temp = expr;
       state.rename(temp, ns);
       do_simplify(temp);

       if(temp.id() == ID_index) {
           // check cache if the index operation was already done
           auto it = op_cache.find(temp);
           if(write || it == op_cache.end()) {
               handle_index(to_index_expr(temp), state, write);
               op_cache[temp] = 0;
           }
       }
   }
   goto_symext::dereference_rec(expr, state, guard, write);
}