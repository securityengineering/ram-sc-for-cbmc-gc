//
// Created by weber on 27.07.2017.
//
#include "csvFileWriter.h"
//#include <boost/filesystem.hpp>

void CSVFileWriter::writeFiles() {
    if(!pathname.empty())
        checkForDirectories();

    writeGates();
    writeTraffic();
    writeRounds();
    writeTime();
}

void CSVFileWriter::checkForDirectories() {
    /*boost::filesystem::path dir(pathname);

    if(!(boost::filesystem::exists(dir))){
        if (!boost::filesystem::create_directory(dir))
            std::cout << "error creating new directory" << std::endl;
    }*/
}

void CSVFileWriter::writeGates() {
    std::ofstream gateFile;
    gateFile.open (filename + "_gates.csv");
    gateFile << outGates;
    gateFile.close();
}

void CSVFileWriter::writeTraffic() {
    std::ofstream trafficFile;
    trafficFile.open (filename + "_traffic.csv");
    trafficFile << outTraffic;
    trafficFile.close();
}

void CSVFileWriter::writeRounds() {
    std::ofstream roundsFile;
    roundsFile.open (filename + "_rounds.csv");
    roundsFile << outRounds;
    roundsFile.close();
}

void CSVFileWriter::writeTime() {
    std::ofstream timeFile;
    timeFile.open (filename + "_time.csv");
    timeFile << outTime;
    timeFile.close();
}

void CSVFileWriter::addLine(uint64_t lineX) {
    outGates += "\n" + std::to_string(lineX);
    outTraffic += "\n" + std::to_string(lineX);
    outRounds += "\n" + std::to_string(lineX);
    outTime += "\n" + std::to_string(lineX);
}

void CSVFileWriter::addHeader(std::string header) {
    outGates += header;
    outTraffic += header;
    outRounds += header;
    outTime += header;
}

void CSVFileWriter::addEmpty() {
    outGates +=  ", ";
    outTraffic +=  ", ";
    outRounds +=  ", ";
    outTime +=  ", ";
}

void CSVFileWriter::addOutType(minSettings settings, double ms) {
    addOutType(*settings.out, ms);
    delete settings.out;
}
/** Result is divided by ms, default is 1000, which outputs results in s
 */
void CSVFileWriter::addOutType(outType out, double ms) {
    if(out.gates == UINT64_MAX)
        outGates += ", ";
    else outGates +=  ", " + std::to_string(out.gates);

    if(out.traffic == UINT64_MAX)
        outTraffic += ", ";
    else outTraffic +=  ", " + std::to_string(out.traffic / 8);

    outRounds +=  ", " + std::to_string(out.rounds);

    if(needsTime(out) == -1)
        outTime += ", ";
    else outTime +=  ", " + std::to_string(needsTime(out) / (ms));//*3600.0*24.0*30.0));
}
