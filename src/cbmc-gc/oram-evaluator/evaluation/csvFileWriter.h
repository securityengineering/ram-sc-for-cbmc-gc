//
// Created by weber on 27.07.2017.
//

#ifndef ORAMEVALUATOR_FILEWRITER_H
#define ORAMEVALUATOR_FILEWRITER_H

#include <list>
#include <iostream>
#include <fstream>
#include <utility>
#include "../types/outType.h"
#include "../types/minSettings.h"

class CSVFileWriter {
private:
    std::string outGates;
    std::string outTraffic;
    std::string outRounds;
    std::string outTime;
    std::string filename;
    std::string pathname;
public:
    explicit CSVFileWriter(std::string filename) : filename(std::move(filename)), pathname("") { }
    explicit CSVFileWriter(std::string filename, std::string pathname) : filename(std::move(filename)), pathname(std::move(pathname)) { }

    void addLine(uint64_t lineX);
    void addHeader(std::string header);
    void addEmpty();
    void addOutType(outType out, double ms=1000);
    void addOutType(minSettings settings, double ms=1000);

    void writeFiles();
    void checkForDirectories();

    void writeGates();
    void writeTraffic();
    void writeRounds();
    void writeTime();
};

#endif //ORAMEVALUATOR_FILEWRITER_H
