RAM-SC Extension for CBMC-GC v2

OVERVIEW
========

This release contains the RAM-SC extension for CBMC-GC v2 [1] as well as an ORAM runtime prediction library, which are both described in our paper [2]. Warning: The code is highly experimental and should never be used in any productive environment. This compiler DOES NOT create ready-to-use RAM-SC programs, cf. [2]. Nevertheless, it might be useful for researchers to examine the efficiency of RAM-SC for a given ANSI C code. Feel free to contact us in case of technical/research questions, but keep in mind that we unfortunately do not have resources to provide any form of user support.

INSTRUCTIONS
============
This extensions hooks deeply into CBMC-GC. Because of this this project contains a snapshot of CBMC-GC itself. For compilation of the compiler follow the instructions given in [1]. Similarly, when compiling a RAM-SC program, use the same procedure as when compiling circuits with CBMC-GC.
The major difference between CBMC-GC and this extension is the output printed to stdout. With this extension, all array accesses in the input source code are studied and an optimal ORAM scheme is identified for each array according the criteria described in [2]. The output of the compiler is self-explanatory and verbose.

The ORAM library (src/cbmc-gc/src/oram-evaluator) can also be compiled and used without CBMC-GC. Place it in a separate directory and run cmake.


[1] https://gitlab.com/securityengineering/CBMC-GC-2

[2] Towards Practical RAM Based Secure Computation, Niklas Büscher, Alina Weber, Stefan Katzenbeisser, ESORICS 2018




