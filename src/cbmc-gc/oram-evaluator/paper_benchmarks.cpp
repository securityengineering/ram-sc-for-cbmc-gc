//
// Created by alina on 30.08.17.
//

#include "paper_benchmarks.h"
#include "evaluation/csvFileWriter.h"
#include "evaluation/access_functions.h"

void test_GKK() {
    std::cout << "GKK Benchmarks: use old formulas, no fixed bucket-sizes, no counter, and no dynamic block-size" << std::endl;
    for(uint16_t d = 20; d >= 16; d--)
        ORAMFactory::acc_GKK_BT(d, 512, 16);
}

void test_CORAM() {
    std::cout << "Circuit ORAM Benchmarks: use new formulas, counter and dynamic block-size" << std::endl;
    ORAMFactory::acc_LSO_old(1, 30, 32);
    ORAMFactory::acc_BT(30, 32, 120, 16, 8, false);
    ORAMFactory::acc_BT(30, 32, 150, 16, 8, true);
    ORAMFactory::acc_Path("Path", 30, 32, 4, 16, 89, 8);
    ORAMFactory::acc_Path("PathSC", 30, 32, 4, 16, 89, 8);
    ORAMFactory::acc_Path("Scoram", 30, 32, 6, 16, 151, 8);
    ORAMFactory::acc_Path("CORAM", 30, 32, 3, 16, 89, 8);
    ORAMFactory::acc_Path("CORAM", 30, 32, 3, 16, 33, 8);
    ORAMFactory::acc_Path("CORAM", 30, 32, 2, 16, 55, 8);

    auto* oram = new Coram(pow(2, 6), 32, 6, 3, 16, 32);
    oram->build(8);
    outType& out = oram->c_acc(32);
    std::cout << "test" << ": d = " << 29 << " b = " << 32 << " B = " << 3 << " c = " << 16 << " stash = " << 32 << ": " << out << std::endl;
    delete &out;
}

void plot_add_TLS(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = TrivialLinearScan::c_write_old(m, b);
    writer->addOutType(out);
    delete &out;
}

void plot_add_new_TLS(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = TrivialLinearScan::c_write(m, b);
    writer->addOutType(out);
    delete &out;
}

void plot_add_BT(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = acc_BT_slow(1, false, m, b, 120, 16, 8);
    writer->addOutType(out);
    delete &out;
}

void plot_add_Path(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = acc_Path_slow(1, false, m, b, 4, 16, 89, 8);
    writer->addOutType(out);
    delete &out;
}

void plot_add_PathSC(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = acc_PathSC_slow(1, false, m, b, 4, 16, 89, 8);
    writer->addOutType(out);
    delete &out;
}

void plot_add_SCORAM(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = acc_SCORAM_slow(1, false, m, b, 6, 16, StandardScoramS(myLog2(m)).min, 8);
    writer->addOutType(out);
    delete &out;
}

void plot_add_CORAM(uint64_t m, uint64_t b, CSVFileWriter *writer) {
    outType& out = acc_CORAM_slow(1, false, m, b, 3, 8, 33, 8);
    writer->addOutType(out);
    delete &out;
}

void plot_add_SQORAM(uint64_t m, uint64_t b, uint64_t acc, CSVFileWriter* writer) {
    auto T = (uint16_t) ceil(sqrt(m * myLog2(m) - m + 1));
    outType& out = divideWR(acc_OSQR_slow(acc, false, m, b, T, 8, UINT16_MAX), acc);
    writer->addOutType(out);
    delete &out;
}

void SQORAM_add_CORAM(uint64_t m, uint64_t b, CSVFileWriter* writer) {
    SQR_CORAM* path = (SQR_CORAM*) ORAMFactory::create_Path("SQR_CORAM", m, b, 3, 8, 33, UINT16_MAX);
    outType& out = path->c_acc(b);
    writer->addOutType(out);
    delete &out;
    delete path;
}

void SQORAM_add_SQORAM(uint64_t m, uint64_t b, uint64_t acc, CSVFileWriter* writer) {
    auto T = (uint16_t) ceil(sqrt(m * myLog2(m) - m + 1));
    outType& out = divideWR(acc_OSQR_slow(acc, false, m, b, T, 8, UINT16_MAX), acc);
    //outType& out = acc_OSQR_slow(1, false, m, b, 8, UINT16_MAX);
    writer->addOutType(out);
    delete &out;
}

void SQORAM_add_FLORAM_ORIG(uint64_t m, uint64_t b, uint64_t acc, CSVFileWriter* writer) {
    outType& bounds = (3040-myLog2(m)*96)*c_lin_gate() + (32-myLog2(m)-1)*c_lin_gate();
    outType& out = bounds + divideWR(acc_FLORAM_Orig(acc, false, m, b, 8, UINT16_MAX), acc);
    writer->addOutType(out);
    delete &out;
}

void SQORAM_add_FLORAM(uint64_t m, uint64_t b, uint64_t acc, CSVFileWriter* writer) {
    outType& out = divideWR(acc_FLORAM(acc, false, m, b, 8, UINT16_MAX), acc);
    writer->addOutType(out);
    delete &out;
}


void SQORAM_add_CPRG_FLORAM(uint64_t m, uint64_t b, uint64_t acc, CSVFileWriter* writer) {
    outType& out = divideWR(acc_FLORAM_CPRG_slow(acc, false, m, b, 8, UINT16_MAX), acc);
    writer->addOutType(out);
    delete &out;
}

void SQORAM_add_CPRG_FLORAM_ORIG(uint64_t m, uint64_t b, uint64_t acc, CSVFileWriter* writer) {
    // check if index within bounds
    outType& bounds = (3040-myLog2(m)*96)*c_lin_gate() + (32-myLog2(m)-1)*c_lin_gate();
    outType& out = bounds + divideWR(acc_FLORAM_CPRG_Orig(acc, false, m, b, 8, UINT16_MAX), acc);

    writer->addOutType(out);
    delete &out;
}

void plot_SQORAM() {
    // set hardware parameters
    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);

    CSVFileWriter* writer = new CSVFileWriter("SQR");
    writer->addHeader("d, TLS 128, SQR 128, CORAM 128");
    for(uint16_t d = 2; d <= 20; d++) {
        writer->addLine(d);
        auto m = (uint64_t) pow(2, d);
        auto acc = (uint64_t) (floor(sqrt(m * d)));

        plot_add_TLS(m, 128, writer);
        SQORAM_add_SQORAM(m, 128, acc, writer);
        SQORAM_add_CORAM(m, 128, writer);
        std::cout << "Plotted d = " << d << std::endl;
    }
    writer->writeFiles();
    delete writer;
}

void plot_scalingORAM() {
    // set parameters
    Settings::get().set(0.5, 500, 10000*8, 80);
    Settings::get().setYao(80);
    uint16_t b = 32;


    CSVFileWriter* writer = new CSVFileWriter("scalingORAM");
    writer->addHeader("d, TLS, SQR, CORAM, FLORAM, FLORAM OPT, FLORAM CPRG, FLORAM CPRG OPT");
    for(uint16_t d = 5; d <= 30; d++) {
        writer->addLine(d);
        auto m = (uint64_t) pow(2, d);

        uint64_t myAccSQR = (uint64_t) ceil(sqrt(m * d - m + 1)) + 1;
        auto c = (uint16_t) (b > 128? 1 : floor(128/b));
        auto floramAcc = (uint64_t) ceil(sqrt(m/c)/8);

        if(d <= 20)
            plot_add_new_TLS(m, b, writer);
        else writer->addEmpty();

        if(d < 22) {
            SQORAM_add_SQORAM(m, b, myAccSQR, writer);
            SQORAM_add_CORAM(m, b, writer);
        }
        else {
            writer->addEmpty();
            writer->addEmpty();
        }
        SQORAM_add_FLORAM_ORIG(m, b, floramAcc, writer);
        SQORAM_add_FLORAM(m, b, floramAcc, writer);
        SQORAM_add_CPRG_FLORAM_ORIG(m, b, floramAcc, writer);
        SQORAM_add_CPRG_FLORAM(m, b, floramAcc, writer);
        std::cout << "Plotted d = " << d << std::endl;
    }

    writer->writeFiles();
    delete writer;
}

void plot_SCORAM() {
    CSVFileWriter* writer = new CSVFileWriter("SCORAM");
    writer->addHeader("d, TLS, BT, Path, Path-SC, SCORAM");
    for(uint16_t d = 10; d <= 29; d++) {
        writer->addLine(d);
        auto m = (uint64_t) pow(2, d);

        if(d <= 20)
            plot_add_TLS(m, 32, writer);
        else writer->addEmpty();

        plot_add_BT(m, 32, writer);
        plot_add_Path(m, 32, writer);
        plot_add_PathSC(m, 32, writer);
        plot_add_SCORAM(m, 32, writer);

        std::cout << "Plotted d = " << d << std::endl;
    }
    writer->writeFiles();
    delete writer;
}

void run_benchmarks() {
    //measure(test_GKK);
    //measure(test_CORAM);
    //measure(plot_SCORAM);
    //measure(plot_SQORAM);
    measure(plot_scalingORAM);
}