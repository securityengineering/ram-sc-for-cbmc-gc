//
// Created by anon on 20.09.17.
//

#ifndef CBMC_GC_BV_CBMC_RAMSC_H
#define CBMC_GC_BV_CBMC_RAMSC_H

#include "bv_cbmc_gc.h"
#include "oram_goto_symext.h"

class bv_cbmc_ramsc : public bv_cbmc_gct {

public:
    struct oram_instancet {
        oramt* oram;

        // default LS operations
        bvt (bv_cbmc_ramsc::*declaration)(/* TODO */) = &bv_cbmc_ramsc::dummy_func;
        bvt (bv_cbmc_ramsc::*initialization)(const exprt &expr) = &bv_cbmc_gct::convert_array_initialization;
        bvt (bv_cbmc_ramsc::*member_declaration)(const member_exprt &expr, const typet &struct_op_type, const bvt &struct_bv)
                = &bv_cbmc_gct::convert_member_declarate;

        bvt (bv_cbmc_ramsc::*constRead)(exprt array, mp_integer index_value) = &bv_cbmc_gct::convert_read_constant;
        void (bv_cbmc_ramsc::*constWrite)(mp_integer size, mp_integer op1_value, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv)
            = &bv_cbmc_ramsc::convert_write_constant;

        bvt (bv_cbmc_ramsc::*secretRead)(const exprt expr, std::size_t width, mp_integer array_size, const bvt &array_bv)
            = &bv_cbmc_gct::convert_read_dynamic;
        void (bv_cbmc_ramsc::*secretWrite)(mp_integer size, const exprt &op1, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv)
            = &bv_cbmc_gct::convert_write_dynamic;

        oram_instancet(oramt* oram) : oram(oram) { }
    };

    typedef std::unordered_map<std::string, oram_instancet*> instanceMapping;

private:
    oramMapping& oramMap;
    instanceMapping instanceMap{};
    uint8_t array_rec_level = 0;

    inline bvt dummy_func() { return bvt(); }
public:
    bv_cbmc_ramsc(const namespacet& _ns, propt& _prop, const building_blockst& _conv, expr_optimizationt opt, oramMapping& oramMap)
            : bv_cbmc_gct(_ns, _prop, _conv, opt), oramMap(oramMap) { }

    bvt handle_instantiation(std::string identifier);
    bvt handle_declaration(oram_instancet& instance);

    bvt convert_index(const index_exprt& index) override;
    bvt convert_index_constant(exprt array, mp_integer index_value) override;
    bvt convert_index_dynamic(const index_exprt expr, std::size_t width, mp_integer array_size, const bvt& array_bv) override;

    void count_rec_levels(const exprt &expr);
    void count_rec_levels(const typet &type);

    bvt convert_array(const exprt &expr) override;
    bvt convert_member_declaration(const member_exprt &expr, const typet &struct_op_type, const bvt &struct_bv) override;
    void convert_with_array_constant(
            const array_typet &type, mp_integer size, mp_integer op1_value, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv) override;

    void convert_with_array_dynamic(
            const array_typet &type, mp_integer size, const exprt &op1, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv) override;

    bool check_empty(oramt* oram);
    void clean_up();
};

#endif //CBMC_GC_BV_CBMC_RAMSC_H
