//
// Created by anon on 20.09.17.
//

#include "bv_cbmc_ramsc.h"
#include "oram_eval_interface.h"

bvt bv_cbmc_ramsc::handle_instantiation(std::string identifier) {
    // get oramt
    oramMapping::iterator it = oramMap.find(identifier);
    assert(it != oramMap.end());
    oramt* oram = (*it).second;

    // hand-over to ORAM evaluator
    oram_instancet* instance = evaluate_single(oram);
    instanceMap[identifier] = instance;

    // handle declaration
    return handle_declaration(*instance);
}

bvt bv_cbmc_ramsc::handle_declaration(oram_instancet& instance) {
    return (this->*instance.declaration)();
}

void bv_cbmc_ramsc::count_rec_levels(const exprt &expr) {
    exprt temp = expr;

    while(temp.op0().id() == ID_array) {
        array_rec_level++;
        temp = temp.op0();
    }
}

// this is a array initialization
bvt bv_cbmc_ramsc::convert_array(const exprt &expr) {
    count_rec_levels(expr);

    // get the identifier
    assert(expr.id() == ID_array && expr.type().id() == ID_array);
    std::string identifier = oram_goto_symext::get_identifier_string(expr.type().get("array_ident"), array_rec_level);

    // check if the array was already declared and declare it, if not
    instanceMapping::iterator it = instanceMap.find(identifier);
    if(it == instanceMap.end()) {
        handle_instantiation(identifier);        // TODO: was mit Rückgabewert machen?
        it = instanceMap.find(identifier);
    }

    array_rec_level = 0;

    // call corresponding initialization method
    oram_instancet* instance = (*it).second;
    return (this->*instance->initialization)(expr);
}

// this might be an array declaration via a struct
bvt bv_cbmc_ramsc::convert_member_declaration(const member_exprt &expr, const typet &struct_op_type,
                                              const bvt &struct_bv) {
    assert(expr.op0().id() == ID_symbol);
    // get the identifier
    std::string identifier = oram_goto_symext::get_identifier_string(expr.op0().get("array_ident"), array_rec_level);

    // check if the array was already declared and declare it, if not
    instanceMapping::iterator it = instanceMap.find(identifier);
    if(it == instanceMap.end()) {
        handle_instantiation(identifier);
        it = instanceMap.find(identifier);
    }

    // call corresponding initialization method
    oram_instancet* instance = (*it).second;
    bvt declaration = (this->*instance->member_declaration)(expr, struct_op_type, struct_bv);

    // check if the array should also be initialized
    if(instance->oram->initValues)
        (this->*instance->initialization)(expr);        // TODO: was mit Rückgabewert machen?

    return declaration;
}


void bv_cbmc_ramsc::count_rec_levels(const typet &type) {
    typet temp = type;

    while(temp.has_subtype() && temp.subtypes().at(0).id() == ID_array) {
        array_rec_level++;
        temp = temp.subtypes().at(0);
    }
}

void bv_cbmc_ramsc::convert_with_array_constant(
        const array_typet &type, mp_integer size, mp_integer op1_value, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv) {

    // get the identifier
    count_rec_levels(type);
    std::string identifier = oram_goto_symext::get_identifier_string(type.get("array_ident"), array_rec_level);
    // reset level
    array_rec_level = 0;

    // check if the array was already declared and declare it, if not
    instanceMapping::iterator it = instanceMap.find(identifier);
    if(it == instanceMap.end()) {
        handle_instantiation(identifier);
        it = instanceMap.find(identifier);
    }

    // call corresponding constant write method
    oram_instancet* instance = (*it).second;
    instance->oram->constWrite--;
    return (this->*instance->constWrite)(size, op1_value, op2_bv, prev_bv, next_bv);
}

void bv_cbmc_ramsc::convert_with_array_dynamic(
        const array_typet &type, mp_integer size, const exprt &op1, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv) {

    // get the identifier
    count_rec_levels(type);
    std::string identifier = oram_goto_symext::get_identifier_string(type.get("array_ident"), array_rec_level);
    // reset level
    array_rec_level = 0;

    // check if the array was already declared and declare it, if not
    instanceMapping::iterator it = instanceMap.find(identifier);
    if(it == instanceMap.end()) {
        handle_instantiation(identifier);
        it = instanceMap.find(identifier);
    }

    // call corresponding constant write method
    oram_instancet* instance = (*it).second;
    instance->oram->secretWrite--;
    return (this->*instance->secretWrite)(size, op1, op2_bv, prev_bv, next_bv);
}

// this is a read operation with a constant index
bvt bv_cbmc_ramsc::convert_index_constant(exprt array, mp_integer index_value) {

    // get the identifier
    std::string identifier = oram_goto_symext::get_identifier_string(array.type().get("array_ident"), array_rec_level);

    // check if the array was already declared and declare it, if not
    instanceMapping::iterator it = instanceMap.find(identifier);
    if(it == instanceMap.end()) {
        handle_instantiation(identifier);
        it = instanceMap.find(identifier);
    }

    if(array_rec_level > 0)
        array_rec_level--;

    // call corresponding constant write method
    oram_instancet* instance = (*it).second;
    instance->oram->constRead--;
    return (this->*instance->constRead)(array, index_value);
}

// this is a read operation with a dynamic (secret) index
bvt bv_cbmc_ramsc::convert_index_dynamic(const index_exprt expr, std::size_t width, mp_integer array_size, const bvt &array_bv) {

    // get the identifier
    std::string identifier = oram_goto_symext::get_identifier_string(expr.op0().type().get("array_ident"), array_rec_level);

    // check if the array was already declared and declare it, if not
    instanceMapping::iterator it = instanceMap.find(identifier);
    if(it == instanceMap.end()) {
        handle_instantiation(identifier);
        it = instanceMap.find(identifier);
    }

    if(array_rec_level > 0)
        array_rec_level--;

    // call corresponding constant read method
    oram_instancet* instance = (*it).second;
    instance->oram->secretRead--;
    return (this->*instance->secretRead)(expr.index(), width, array_size, array_bv);
}

bvt bv_cbmc_ramsc::convert_index(const index_exprt &index) {
    if(index.op0().id() == ID_index) {
        array_rec_level++;
        return bv_cbmc_gct::convert_index(index);
    }
    else return bv_cbmc_gct::convert_index(index);
}

bool bv_cbmc_ramsc::check_empty(oramt* oram) {
    bool ret = oram->constRead == 0 && oram->constWrite == 0 && oram->secretRead == 0 && oram->secretWrite == 0;

    if(!ret)
        std::cout << "\n" << *oram << "is not empty!\n" << std::endl;

    return ret;
}

void bv_cbmc_ramsc::clean_up() {
    for(auto it = instanceMap.begin(); it != instanceMap.end(); ++it) {
        assert(check_empty((*it).second->oram));
        delete (*it).second->oram;
    }
    instanceMap.clear();
    oramMap.clear();
}