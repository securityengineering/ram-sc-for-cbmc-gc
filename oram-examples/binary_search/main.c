#include <inttypes.h>

#define LOGN 10
#define N 1 << LOGN
#define A 16

typedef struct {
	int m[N];
} MyArray;

int mpc_main(MyArray INPUT_A, int INPUT_B) {
	
	for(int j = 0; j < A; j++) {
		int key = INPUT_B + j;
		int p1 = 0;			// pointer to first element of subarray
		int p2 = N-1;			// pointer to last element of subarray
	
		int pm = (p1 + p2) >> 1;	// pointer to middle element

		int i = 0;			// bound looping (maximum N steps)

		while(p1 <= p2 && i++ < LOGN) {
			if(INPUT_A.m[pm] < key)
				p1 = pm + 1;	// element has to be in the right half
			else if(INPUT_A.m[pm] == key)
				return pm;
			else p2 = pm - 1;	// element has to be in the left half

			pm = (p1 + p2) >> 1;
		}
	}
	// element is not in the array
	return -1;
}
