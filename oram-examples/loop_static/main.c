int mpc_main(int INPUT_A, int INPUT_B) {
	short array[4] = {0, 1, 2, 3};
	int out = 0;

	for(int i = 0; i < 4; i++) {
		out += array[i];
	}
	
	return out + INPUT_B;
}
