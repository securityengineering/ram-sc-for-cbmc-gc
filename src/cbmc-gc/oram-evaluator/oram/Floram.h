//
// Created by weber on 17.01.2018.
//

#ifndef ORAM_EVALUATOR_FLORAM_H
#define ORAM_EVALUATOR_FLORAM_H

#include "oram.h"
#include <algorithm>

double interpolate5(double from, double to, int x);

class FloramOrig : public ORAM {
protected:
    uint64_t t;                     // number accesses this period
    uint16_t c;                     // packing factor
    uint64_t numB;                  // number of blocks
    uint16_t blockMul;              // number of blocks for one data element
    uint64_t T;                     // period
public:
    FloramOrig(uint64_t m, uint64_t b, uint16_t dummy) :            // TODO c aus Konstruktor entfernen
            ORAM(m, b, (uint64_t) std::max((int) b, 128), "Floram"), t(0),
            c((uint16_t) (b > 128? 1 : floor(128/b))), numB(m/c), blockMul((uint16_t) std::max(1, (int) ceil(b/128))),
            T((uint64_t) ceil(sqrt(numB)/8)) { }

    outType& c_init(bool values) override;
    outType& c_acc(uint64_t b) override;
    virtual outType& c_acc(uint64_t noAcc, uint64_t b);
    outType& c_amortized(uint64_t noAcc, bool values) override;
    virtual outType& c_read_static();
    virtual outType& c_write_static();
    virtual outType& c_read();

    inline void setSteps(uint64_t steps) { t = steps; }
    inline uint64_t getT() { return T; }

protected:
    virtual outType& fss_getblockvector_with_callback(bool readOnly);
    virtual outType& fss_traverselevels(bool readOnly);
    virtual outType& floram_scan_callback(bool readOnly);
    virtual outType& c_refresh();

    virtual outType& scanrom_read_with_bitvector();
    virtual outType& scanrom_read_with_blockvector();
    virtual outType& scanwrom_read();
    virtual outType& scanwrom_write_with_blockvector();
};

class FloramCPRGOrig : public FloramOrig {

public:
    FloramCPRGOrig(uint64_t m, uint64_t b, uint16_t dummy) : FloramOrig(m, b, dummy) { }

protected:
    outType& fss_getblockvector_with_callback(bool smaller) override;
    outType& fss_traverselevels(bool readOnly) override;
};

class Floram : public FloramOrig {
public:
    Floram(uint64_t m, uint64_t b, uint16_t dummy) : FloramOrig((uint64_t) pow(2, myLog2(m)), (uint64_t) pow(2, myLog2(b)), dummy) { }

    outType& c_acc(uint64_t b) override;
    outType& c_acc(uint64_t noAcc, uint64_t b) override;
    outType& c_write_static() override;
    outType& c_read() override;
	  static double_t estimate_local_computation_time(uint32_t d);

private:
    outType& fss_getblockvector_with_callback(bool readOnly) override;
    outType& fss_traverselevels(bool readOnly) override;
    outType& floram_scan_callback(bool readOnly) override;
    outType& scanrom_read_with_bitvector() override;
    outType& scanrom_read_with_blockvector() override;
    outType& scanwrom_write_with_blockvector() override;
    outType& scanwrom_read() override;
};

class FloramCPRG : public FloramCPRGOrig {
public:
    FloramCPRG(uint64_t m, uint64_t b, uint16_t dummy) :
            FloramCPRGOrig((uint64_t) pow(2, myLog2(m)), (uint64_t) pow(2, myLog2(b)), dummy) { }

    outType& c_acc(uint64_t b) override;
    outType& c_acc(uint64_t noAcc, uint64_t b) override;
    outType& c_write_static() override;
    outType& c_read() override;
		static double_t estimate_local_computation_time(uint32_t d);
private:
    outType& fss_getblockvector_with_callback(bool readOnly) override;
    outType& fss_traverselevels(bool readOnly) override;
    outType& floram_scan_callback(bool readOnly) override;
    outType& scanrom_read_with_bitvector() override;
    outType& scanrom_read_with_blockvector() override;
    outType& scanwrom_write_with_blockvector() override;
    outType& scanwrom_read() override;
};

#endif //ORAM_EVALUATOR_FLORAM_H
