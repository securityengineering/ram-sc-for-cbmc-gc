#include <inttypes.h>

#define LOGN 5
#define N 1 << LOGN 

typedef struct {
	short m[N][N];
} MyArray;

int mpc_main(MyArray INPUT_A_e, int INPUT_B_s, int INPUT_B_t) {

	int vis[N];		// indicates if node has been visited
	int dis[N];		// indicates current smallest distance from source to each other node

	// initialize arrays to zero (memset not implemented yet)
	for(int i = 0; i < N; ++i) {
		vis[i] = 0;
		dis[i] = 0;
	}

	vis[INPUT_B_s] = 1;
	for(int i = 0; i < N; ++i)
		dis[i] = INPUT_A_e.m[INPUT_B_s][i];

	for(int i = 1; i < N; ++i) {
		int bestj = -1;
		for(int j = 0; j < N; ++j) {
			if(!vis[j] && (bestj < 0 || dis[j] < dis[bestj]))
				bestj = j;	
		}
		vis[bestj] = 1;		
		for(int j = 0; j < N; ++j) {
			if(!vis[j] && (dis[bestj] + INPUT_A_e.m[bestj][j] < dis[j]))
				dis[j] = dis[bestj] + INPUT_A_e.m[bestj][j];
		}									
	}		
	return dis[INPUT_B_t];
}
