//
// Created by alina on 31.08.17.
//

#ifndef ORAMEVALUATOR_OUTTYPE_H
#define ORAMEVALUATOR_OUTTYPE_H

#include <cstdint>
#include <cmath>
#include <algorithm>
//#include <values.h>
#include "../settings.h"
#include "../helper.h"

struct outType {
    uint64_t gates{UINT64_MAX};
    uint64_t traffic{UINT64_MAX};
    uint64_t rounds{UINT64_MAX};
    double_t local_time;

    outType() = default;
    outType(uint64_t g, uint64_t t, uint64_t r) : gates(g), traffic(t), rounds(r), local_time(0) { }

    inline outType& operator+= (const outType& b) {
        if(gates + b.gates < gates || gates + b.gates < b.gates)
            gates = UINT64_MAX;
        else gates += b.gates;

        if(traffic + b.traffic < traffic || traffic + b.traffic < b.traffic)
            traffic = UINT64_MAX;
        else traffic += b.traffic;
        rounds = std::max(rounds, b.rounds);

				local_time += b.local_time;

        delete &b;

        return *this;
    }

    inline outType& addWR(const outType& b) {
        if(gates + b.gates < gates || gates + b.gates < b.gates)
            gates = UINT64_MAX;
        else gates += b.gates;

        if(traffic + b.traffic < traffic || traffic + b.traffic < b.traffic)
            traffic = UINT64_MAX;
        else traffic += b.traffic;

        rounds += b.rounds;
        
        local_time += b.local_time;

        delete &b;

        return *this;
    }
};

inline double_t needsTime(const outType& outT) {
    if(outT.gates == UINT64_MAX || outT.traffic == UINT64_MAX || outT.rounds == UINT64_MAX)
        return -1;

    double_t rounds = Settings::get().getLatency() * outT.rounds;               // calculation is in ms
    double_t traffic = outT.traffic / (1000*Settings::get().getBandwidth());    // Settings bandwidth is in Mbps, need bit/ms
    double_t gates = outT.gates /  Settings::get().getCalc();

    /*double_t out1 = rounds + traffic;
    double_t out = out1 + gates;
    if(out1 < rounds || out1 < traffic || out < out1 || out < gates)
        return -1;*/
    double_t out1 = std::max(gates, traffic);
    double_t out = out1 + rounds;
    if(out < out1)
        return -1;
    
    if(outT.local_time > 0) {
			out += outT.local_time;
			//std::cout << "adding " << outT.local_time << std::endl;
		}

    return out;
}

inline std::ostream& operator<<(std::ostream& ostr, const outType& i) {
    return ostr << "gates: " << i.gates << " traffic: " << i.traffic << " rounds: " << i.rounds << " time: " << std::to_string(needsTime(i) / 1000)<< " s";
}

inline std::ostream& operator<<(std::ostream& ostr, const outType* i) {
    return ostr << "gates: " << i->gates << " traffic: " << i->traffic << " rounds: " << i->rounds << " time: " << std::to_string(needsTime(*i) / 1000) << " s";
}

inline outType& operator+ (const outType& a, const outType& b) {
    auto* out = new outType;
    *out = {a.gates + b.gates, a.traffic + b.traffic, std::max(a.rounds, b.rounds)};
		out->local_time = a.local_time + b.local_time;

    if(out->gates < a.gates || out->gates < b.gates)
        out->gates = UINT64_MAX;

    if(out->traffic < a.traffic || out->traffic < b.traffic)
        out->traffic = UINT64_MAX;

    delete &a;
    delete &b;

    return *out;
}

inline outType& addWR(const outType &a, const outType &b) {
    uint64_t rounds = a.rounds + b.rounds;
    outType& out = a + b;
    out.rounds = rounds;

    return out;
}

inline outType& operator- (const outType& a, const outType& b) {
    auto* out = new outType;
    *out = {a.gates - b.gates, a.traffic - b.traffic, std::max(a.rounds, b.rounds)};
		out->local_time = a.local_time - b.local_time;

    delete &a;
    delete &b;

    return *out;
}

inline outType& minusWR(const outType &a, const outType &b) {
    uint64_t rounds = a.rounds - b.rounds;
    outType& out = a - b;
    out.rounds = rounds;

    return out;
}

inline outType& operator* (const uint64_t m, const outType& b) {
    auto* out = new outType;
    *out = {m*b.gates, m*b.traffic, b.rounds};
    out->local_time = (double)m *  b.local_time;

    if(myLog2(m) + myLog2(b.gates) > 64)
        out->gates = UINT64_MAX;

    if(myLog2(m) + myLog2(b.traffic) > 64)
        out->traffic = UINT64_MAX;

    delete &b;

    return *out;
}

inline outType& multiplyWR(const uint64_t m, const outType &b) {
    uint64_t rounds = m * b.rounds;
    outType& out = m * b;
    out.rounds = rounds;

    return out;
}

inline outType& operator/ (const outType& b, const uint64_t d) {
    auto* out = new outType;
    *out = {(uint64_t)ceil(b.gates/d), (uint64_t)ceil(b.traffic/d), d};
    out->local_time = b.local_time/(double) d;
    delete &b;

    return *out;
}

inline outType& divideWR(const outType& b, const uint64_t d) {
    auto rounds = (uint64_t) ((b.rounds / d) + (b.rounds % d == 0? 0 : 1));

    outType& out = b / d;
    out.rounds = rounds;

    return out;
}

inline bool operator< (const outType& a, const outType& b) {
    switch(Settings::get().getEvalTarget()) {
        case EvalTarget::BANDWIDTH:
            return a.traffic < b.traffic;
        case EvalTarget::CIRCUIT_SIZE:
            return a.gates < b.gates;
        case EvalTarget::ROUNDS:
            return a.rounds < b.rounds;
        default:
            auto time_a = needsTime(a);
            auto time_b = needsTime(b);
            if(time_a == -1)
                return false;
            if(time_b == -1)
                return true;
            return needsTime(a) < needsTime(b);
    }
}


#endif //ORAMEVALUATOR_OUTTYPE_H
