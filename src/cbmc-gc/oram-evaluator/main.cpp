#include <iostream>
#include "fast_calculator.h"
#include "evaluation/evaluator.h"
#include "evaluation/plotter.h"
#include "oram/circuit_oram_orig.h"

void evaluate() {
    std::cout << "Evaluation: 2^30 elements of 32 bit" << std::endl;
    auto* eval = new Evaluator();
 //   eval->evaluate_acc_exact(1, 0, (uint64_t) pow(2, 30), 32);
    delete eval;
}

void plot()  {
    std::cout << "Plot results: 2^5 to 2^24 elements of 128 bit" << std::endl;
    auto* plotter = new Plotter();
    plotter->plot_acc_elements(evalParam{5, 24, 1, 1}, 128, "access");
    delete plotter;
}


/*void plot_floram_orig() {
	evalParam eval = {5, 25, 1, 1};
	Settings::get().setYao(80);
	Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
	std::cout <<acc_FLORAM_CPRG_slow(floramAcc, values, m, b, UINT16_MAX, UINT16_MAX), floramAcc);
}*/

void plot_for_paper() {
    evalParam eval = {5, 25, 1, 1}; //25
    auto* plotter = new Plotter();
    Settings::get().setYao(80);

    /*Settings::get().set(0.5, 500, 10000*8, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("paper");
    plotter->plot_acc_elements(eval, 32, "scalingORAM_opt");*/

    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_80/DC");
    plotter->plot_acc_elements(eval, 32, "access_32_DC");
    plotter->plot_acc_elements(eval, 64, "access_64_DC");
    plotter->plot_acc_elements(eval, 128, "access_128_DC");
    plotter->plot_acc_elements(eval, 1024, "access_1024_DC");

    Settings::get().set(5, 1000, 10000*4, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_80/LAN");
    plotter->plot_acc_elements(eval, 32, "access_32_LAN");
    plotter->plot_acc_elements(eval, 64, "access_64_LAN");
    plotter->plot_acc_elements(eval, 128, "access_128_LAN");
    plotter->plot_acc_elements(eval, 1024, "access_1024_LAN");

    Settings::get().set(50, 200, 10000*4, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_80/WAN");
    plotter->plot_acc_elements(eval, 32, "access_32_WAN");
    plotter->plot_acc_elements(eval, 64, "access_64_WAN");
    plotter->plot_acc_elements(eval, 128, "access_128_WAN");
    plotter->plot_acc_elements(eval, 1024, "access_1024_WAN");

    /*Settings::get().setYao(128);

    Settings::get().set(0.5, 500, 10000*8, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("paper");
    plotter->plot_acc_elements(eval, 32, "scalingORAM_opt_128");

    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_128/DC");
    plotter->plot_acc_elements(eval, 32, "access_32_DC_128");
    plotter->plot_acc_elements(eval, 64, "access_64_DC_128");
    plotter->plot_acc_elements(eval, 128, "access_128_DC_128");
    plotter->plot_acc_elements(eval, 1024, "access_1024_DC_128");

    Settings::get().set(5, 1000, 10000*4, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_128/LAN");
    plotter->plot_acc_elements(eval, 32, "access_32_LAN_128");
    plotter->plot_acc_elements(eval, 64, "access_64_LAN_128");
    plotter->plot_acc_elements(eval, 128, "access_128_LAN_128");
    plotter->plot_acc_elements(eval, 1024, "access_1024_LAN_128");

    Settings::get().set(50, 200, 10000*4, 80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_128/WAN");
    plotter->plot_acc_elements(eval, 32, "access_32_WAN_128");
    plotter->plot_acc_elements(eval, 64, "access_64_WAN_128");
    plotter->plot_acc_elements(eval, 128, "access_128_WAN_128");
    plotter->plot_acc_elements(eval, 1024, "access_1024_WAN_128");

    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    Settings::get().setYao(80);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("n_accesses/DC");
    plotter->plot_accesses(eval, getM, 32, "n_accesses_32");
    plotter->plot_accesses(eval, getM, 64, "n_accesses_64");
    plotter->plot_accesses(eval, getM, 128, "n_accesses_128");
    plotter->plot_accesses(eval, getM, 1024, "n_accesses_1024");

    plotter->setPath("nlogn_accesses/DC");
    plotter->plot_accesses(eval, getMlogM, 32, "nlogn_accesses_32");
    plotter->plot_accesses(eval, getMlogM, 64, "nlogn_accesses_64");
    plotter->plot_accesses(eval, getMlogM, 128, "nlogn_accesses_128");
    plotter->plot_accesses(eval, getMlogM, 1024, "nlogn_accesses_1024");

    plotter->setPath("n2_accesses/DC");
    plotter->plot_accesses(eval, getMSquare, 32, "n2_accesses_32");
    plotter->plot_accesses(eval, getMSquare, 64, "n2_accesses_64");
    plotter->plot_accesses(eval, getMSquare, 128, "n2_accesses_128");
    plotter->plot_accesses(eval, getMSquare, 1024, "n2_accesses_1024");

    Settings::get().setEvalTarget(EvalTarget::BANDWIDTH);
    std::cout << Settings::get() << std::endl;
    plotter->setPath("access_yao_80/BW");
    plotter->plot_acc_elements(eval, 32, "access_32_BW");
    plotter->plot_acc_elements(eval, 64, "access_64_BW");
    plotter->plot_acc_elements(eval, 128, "access_128_BW");
    plotter->plot_acc_elements(eval, 1024, "access_1024_BW");*/

    delete plotter;
}

void dijkstra(uint64_t b, std::string net) {
    auto* eval = new Evaluator();

    CSVFileWriter* writer = new CSVFileWriter("dijkstra"+net);
    writer->addHeader("d, vis type, vis, dis type, dis, INPUT_A_l1 type, INPUT_A_l1, INPUT_A_l0 type, INPUT_A_l0");

    for(uint16_t d = 5; d < 24; d++) {
        writer->addLine(d);
        auto m = (uint64_t) pow(2, d);
        // noSecretRead, noConstRead, noSecretWrite, noConstWrite, values?, m, b

        // array vis:
        minSettings vis = eval->evaluate(0, (uint64_t) pow(m, 2), m, m, false, m, b);
        writer->addHeader(", " + std::to_string(vis.type_id));
        writer->addOutType(*vis.out);
        delete vis.out;

        // array dis:
        minSettings dis = eval->evaluate((m-1)*(2*m-1)+1, (m-1)*(2*m-1), (m+1)*m, 0, false, m, b);
        writer->addHeader(", " + std::to_string(dis.type_id));
        writer->addOutType(*dis.out);
        delete dis.out;

        // array INPUT_A_l1
        minSettings l1 = eval->evaluate(0, m*m, 0, 0, true, m, b);
        writer->addHeader(", " + std::to_string(l1.type_id));
        writer->addOutType(*l1.out);
        delete l1.out;

        // array INPUT_A_l0
        minSettings l0 = eval->evaluate(m, 0, 0, 0, true, m, m*b);
        writer->addHeader(", " + std::to_string(l0.type_id));
        writer->addOutType(*l0.out);
        delete l0.out;

        std::cout << "evaluated d: " << d << std::endl;
    }
    delete eval;
    writer->writeFiles();
    delete writer;
}


void plot_accesses() {
    auto* plotter = new Plotter();
    Settings::get().setYao(80);
    
    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    plotter->plot_accesses2(30, 32, "accesses_30_32_DC");
    //plotter->plot_accesses3(12, 32, "accesses_12_32_DC");
    //plotter->plot_accesses3(10, 1024, "accesses_10_1024_DC");
    
//    Settings::get().set(50, 200, 10000*4, 80);
    //plotter->plot_accesses2(30, 1024, "accesses_30_32_WAN");
    //plotter->plot_accesses2(20, 32, "accesses_20_32_DC");
//    plotter->plot_accesses2(25, 32, "accesses_25_32_DC");


    /*Settings::get().set(5, 1000, 10000*4, 80);
    plotter->plot_accesses2(10, 32, "accesses_10_32_LAN");
    plotter->plot_accesses2(15, 32, "accesses_15_32_LAN");
    plotter->plot_accesses2(20, 32, "accesses_20_32_LAN");
    plotter->plot_accesses2(25, 32, "accesses_25_32_LAN");

    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    plotter->plot_accesses2(10, 32, "accesses_10_32_WAN");
    plotter->plot_accesses2(15, 32, "accesses_15_32_WAN");
    plotter->plot_accesses2(20, 32, "accesses_20_32_WAN");
    plotter->plot_accesses2(25, 32, "accesses_25_32_WAN");*/

    delete plotter;
}

void plot_dijkstra() {
    Settings::get().setYao(128);
    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    dijkstra(64, "_64_DC");

    //Settings::get().set(50, 200, 10000*4, 80);
    //dijkstra(64, "_64_WAN");

    /*Settings::get().setYao(128);
    Settings::get().set(0.5, 1.03*1000, 10000*4, 80);
    dijkstra(1024, "_1024_DC");

    Settings::get().set(50, 200, 10000*4, 80);
    dijkstra(1024, "_1024_WAN");*/
}

int main() {
    // set standard parameters (same as Zahur et al.)
    Settings::get().set(0.5, 1.03*1000, 4*10000, 80);
    //measure(evaluate);
    //measure(plot);
    //measure(test_fast_formulas);

    //measure(run_benchmarks);
    //measure(plot_dijkstra);
    //measure(plot_accesses);
    measure(plot_for_paper);

/*    auto* plotter = new Plotter();
    Settings::get().setYao(128);
    Settings::get().set(0.5, 1.03*1000, 10000*4, 128);
    evalParam eval = {10, 10, 1, 1};

    plotter->plot_elements(1, true, eval, 32, "test1");

    plotter->plot_elements(1024, true, eval, 32, "test2");

    plotter->plot_accesses(15, 32, "accesses_15_32_WAN");

    delete plotter;*/

    /*for(uint16_t i = 5; i <= 14; i++) {
        CORAMOrig oram = CORAMOrig((uint64_t) pow(2, i), 32);
        outType& access = oram.access();
        std::cout << "oram orig access: " << access << std::endl;
        CORAMOpt oram2 = CORAMOpt((uint64_t) pow(2, i), 32);
        access = oram2.access();
        std::cout << "oram opt access: " << access << "\n" << std::endl;
    }*/

    return 0;
}
