//
// Created by alina on 25.02.18.
//
#include "Floram.h"
#include "linear_scan_oram.h"

outType& FloramCPRG::c_acc(uint64_t b) {
    // 1. FSS-Generate -> return pair of key values k_a^FSS and k_b^FSS (stash scan automatically)
    outType& out = fss_getblockvector_with_callback(false);

    // 2. if element was not found in stash
    // 2.1 retrieve element from OROM and write directly to current position of stash
    // 2.2 set index of first slot to searched one
    out = out.addWR(scanrom_read_with_bitvector() + c_mux(myLog2(numB)));

    // 3. write new data to subblock within stash (append automatically)
    out += TrivialLinearScan::c_write(c, b);

    // 4. apply f ...
    // 5. store result using OWOM
    out = out.addWR(scanwrom_write_with_blockvector());

    t++;
    return out;
}

outType& FloramCPRG::c_acc(uint64_t noAcc, uint64_t b) {
    outType& mod = 0*c_lin_gate();
    outType& full = 0*c_lin_gate();

    for(uint64_t i = 1; i <= noAcc && i <= T; i++) {
        full = full.addWR(c_acc(b));

        if(i == noAcc % T)
            mod = (outType) {full.gates, full.traffic, full.rounds};
    }

    outType& epochs = multiplyWR((uint64_t) floor(noAcc / T), addWR(full, c_refresh()));
    return addWR(epochs, mod);
}

outType& FloramCPRG::fss_traverselevels(bool readOnly) {
    auto blocks = (uint64_t) (readOnly? (numB+128-1)/128 : numB);
    // return pair of key values k_a^FSS and k_b^FSS (over course of function)
    // 1. let both parties compute z^(j, 0) and z^(j, 1) locally (accumulation of left / right children)

    // 2. for each level of the tree
    // 2.1 compute relevant index of this level -> advice bit alpha_j
    // 2.2 submit accelerator_L and accelerator_R to SC
    outType& out = c_B2Y(1, 128);

    // 3. select correct sum Z from acc_L and acc_R by using the advice bit alpha_j
    out += c_mux(128);

    // 4. compute next advice words (sigma^j, tau^(j, 0), tau^(j, 1))
    // 5. reveal advice bits L and R to both parties
    out = out.addWR(2*(2*c_transmit(1) + c_transmit(128)));         // TODO: reveal to party 2 macht eigentlich keinen Sinn?

    // 6. return rest of keys that have not been revealed before TODO: ???
    return addWR(multiplyWR(myLog2(blocks), out), c_B2Y(blockMul, 128));  // TODO: myLog2(numB) oder -1?
}

outType& FloramCPRG::floram_scan_callback(bool readOnly) {
    if(t > 0) {
        // 1. check if first slot of stash was not already used (used bit)
        // 1.1 copy element of first slot to current slot
        // 1.2 set index of current slot to index of first slot
        outType& out = c_mux((uint64_t) blockMul*128+myLog2(numB));

        // 2. set used bit of first slot
        // 3. for each other element in the stash
            // 3.1 check if index of slot is the searched one
            // 3.1.1 copy element from this slot to the first one
            // 3.1.2 set index of first slot to index of current slot
            // 3.1.3 set used bit of current slot (OR)
            // 3.1.4 set found to true (OR)
        return out + t*(c_comp_eq(myLog2(numB)) + blockMul*c_mux(128) + (readOnly? 0*c_lin_gate() : c_mux(myLog2(numB)) + 2*c_lin_gate()));
    }
    return 0*c_lin_gate();
}

outType& FloramCPRG::scanrom_read_with_bitvector() {
    // 1. locally compute FSS-Eval for each element
    // 2. locally compute v for each element
    // 3. convert read block from Boolean to Yao-Share -> R_i
    outType& out = c_B2Y(blockMul, 128);

    // 4. for each 128bit block (blockmultiple times)
    // 4.1 compute PRF (with keys of a and b)
    // 4.2 XOR PRF to read block (Yao)
    // -> import semantic value into SC
    return out + c_SC_PRF(blockMul);
}

outType& FloramCPRG::scanwrom_write_with_blockvector() {
    // 1. calculate difference to old value, block now contains data delta
    // 2. reveal new block to parties
    // 3. locally compute FSS-Eval for all m to get data to write
    return c_Y2B(blockMul, 128);                                                   // TODO: hier Grund wieso dieses CPRG ne Runde weniger
}

outType& FloramCPRG::fss_getblockvector_with_callback(bool readOnly) {
    return fss_traverselevels(readOnly) + floram_scan_callback(readOnly);
}

outType& FloramCPRG::c_read() {
    // 1. calculate index of block and index within block - for free
    // 2. copy data of pass (126 GA) - for free in unconditional use-case

    // FSS-Gen
    // 3. fgbc: fss get blockvector with callback (25328 GA) + division
    outType& out = fss_getblockvector_with_callback(true);

    // 4. if data not found
    // 4.1 scanrom_read_with_blockvector
    out = out.addWR(scanrom_read_with_blockvector());
    // 4.2 set stashi[0] to -1

    // 6. for each element in the block
    // 6.1 check if this is the right subblock (64 GA)
    // 6.2 copy element from stash to output
    out += c*(c_comp_eq(myLog2(c)) + c_mux(b));
    return out;
}

outType& FloramCPRG::scanwrom_read() {
    // read yaoshare and import into circuit
    return c_B2Y(numB, 128);
}

outType& FloramCPRG::c_write_static() {
    // 1. read block from OWOM
    // 2. update element in stash (if existent)
    // 3. write data to scanwrom (Y2B for free)
    // 3. import block from OWOM to OROM
    return scanwrom_read() + t*(c_comp_eq(myLog2(numB)) + c_mux((uint64_t) blockMul*128)) + 2*c_transmit((uint64_t) blockMul*128);
}

outType& FloramCPRG::scanrom_read_with_blockvector() {
    // 1. calculate index within 128 bit block (byte) and index within byte (bit) - for free
    // 2. for each byte in the 128 bit block
    // 2.1 copy byte to temp block (for free)
    // 2.2 check if this is the correct byte AND check if it is also the correct bit
    outType& out = 128*(c_comp_eq(myLog2(m)) + c_lin_gate());
    // 2.2.1 negate result
    out += 128*c_mux(1);

    // 3. reveal block
    out += 2*c_transmit(128);

    // 4. import local block into SC
    out += c_yaoShare(1, 128);

    // 5. blockmultiple times
    // 5.1 PRG
    out += blockMul*c_SC_PRG();
    // 6. copy block
    return out;
}



double_t FloramCPRG::estimate_local_computation_time(uint32_t d) {
	double res = 0;
	// Estimates are given in ms, taken from Figure 10 in FLORAM technical report
	// Estimates are for 2^   10     15    20   25   30
	//double_t const_est[] = {1.5, 20, 32, 200, 3800};
	//double_t no_local_est[] = {16, 24, 33, 56, 100};
	double_t const_est[] = {1.5, 10, 15, 144, 3700};
if(d>=30) {
		res = (1<<(d-30))*const_est[4]; // 2^(d-30)
	} else if (d>=25) {
		//res = ((double)(d-25)/5.0)*(const_est[4]-const_est[3])+const_est[3];
		res = interpolate5(const_est[3], const_est[4], d-25);
	} else if (d>=20) {
		//res = ((double)(d-20)/5.0)*(const_est[3]-const_est[2])+const_est[2];
		res = interpolate5(const_est[2], const_est[3], d-20);
	}else if (d>=15) {
		//res = ((double)(d-15)/5.0)*(const_est[2]-const_est[1])+const_est[1];
		res = interpolate5(const_est[1], const_est[2], d-15);
	} else if (d>=10) {
		res = interpolate5(const_est[0], const_est[1], d-10);
	} else {
		res = (double)const_est[0]*((double)d/10.0);
	}
	//std::cout << "Floram estimate is: " << res << std::endl;
	return res;
}
