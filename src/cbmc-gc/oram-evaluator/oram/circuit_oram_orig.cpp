//
// Created by alina on 09.03.18.
//

#include "circuit_oram_orig.h"
#include "../primitives.h"

// 1. prepare deepest_depth[stage] and deepest_index[stage]
outType& CORAMOrigRek::prepare() {
    // 1 for each stage (including stash)
    // 1.1 for each element in this bucket
    // 1.1.1 compute the LCA of the corresponding leaf label with the leaf label of the eviction path
    uint16_t lcap1[] = {0, 0, 0, 0, 0, 165, 86, 90, 175, 248, 225, 229, 206, 211, 143}; // TODO
    uint16_t lcap2[] = {0, 0, 0, 0, 0, 71, 82, 85, 171, 175, 220, 224, 197, 205, 136};
    // 1.1.2 check if the result is greater than the currently deepest depth of this stage and the element is not a dummy
    // 1.1.2.1 set deepest_depth to the LCA result and deepest_index to the index of the block within this bucket
    outType& prep = (d*B+s)*(lcap1[d]*c_lin_gate() + c_comp_mag(33) + c_lin_gate() + 2*c_mux(32));
    //std::cout << "prepare: " << prep << std::endl;
    return prep;
}

outType& CORAMOptRek::prepare() {
    // 1 for each stage (including stash)
    // 1.1 for each element in this bucket
    // 1.1.1 compute the LCA of the corresponding leaf label with the leaf label of the eviction path
    outType& lca = 2*c_adder(d) + d*c_lin_gate() + myLog2(d)*(c_comp_eq(d) + c_mux(d+myLog2(d)) + c_adder(myLog2(d)));
    // 1.1.2 check if the result is greater than the currently deepest depth of this stage and the element is not a dummy
    // 1.1.2.1 set deepest_depth to the LCA result and deepest_index to the index of the block within this bucket
    outType& prep = (d*B+s)*(lca + c_comp_mag(myLog2(d+2)) + c_lin_gate() + c_mux(myLog2(d+2))) + d*B*c_mux(myLog2(B)) + s*c_mux(myLog2(s));
    //std::cout << "prepare: " << prep << std::endl;
    return prep;
}

// 2. prepare deepest
outType& CORAMOrigRek::prepare_deepest() {
    // 1. initialize deepest[i] and target[i] with -1
    uint16_t daemlich2[] = {0, 0, 0, 0, 0, 384, 448, 512, 576, 640, 704, 768, 832, 896, 960};
    // 2. initialize src and goal to -1 and stash_empty to true // TODO: ist nicht dummy bit sinnvoller?
    // 3. check if stash is empty and save result in temporary variable
    // 4. if stash is not empty
    // 4.1 set src to 0
    // 4.2 set goal to deepest_depth[0]
    outType& stash = (s-1)*c_lin_gate() + 2*c_mux(32);
    // 2.5 for each other stage
    // 2.5.1 check if goal is greater than current level
    // 2.5.1.1 set deepest[i] to src
    // 2.5.2 check if deepest_depth[i] is greater than goal
    // 2.5.2.1 set goal to deepest_depth[i]
    // 2.5.2.2 set src to i
    outType& deepest = daemlich2[d]*c_lin_gate() + stash + d*(c_comp_mag(32) + c_mux(32) + c_comp_mag(32) + 2*c_mux(32));
    //std::cout << "deepest: " << deepest << std::endl;
    return deepest;
}

outType& CORAMOptRek::prepare_deepest() {
    // 1. initialize deepest[i] and target[i] with -1
    // 2. initialize src and goal to -1 and stash_empty to true
    // 3. check if stash is empty and save result in temporary variable
    // 4. if stash is not empty
    // 4.1 set src to 0
    // 4.2 set goal to deepest_depth[0]
    outType& stash = (s-1)*c_lin_gate() + 2*c_mux(myLog2(d+2));
    // 2.5 for each other stage
    // 2.5.1 check if goal is greater than current level
    // 2.5.1.1 set deepest[i] to src
    // 2.5.2 check if deepest_depth[i] is greater than goal
    // 2.5.2.1 set goal to deepest_depth[i]
    // 2.5.2.2 set src to i
    outType& deepest = stash + d*(2*c_comp_mag(myLog2(d+2)) + 3*c_mux(myLog2(d+2)));
    //std::cout << "deepest: " << deepest << std::endl;
    return deepest;
}

// 3. prepare target
outType& CORAMOrigRek::prepare_target() {
    // 1 initialize dest and src to -1
    // 2 for each stage (from leaf to stash)
    // 2.1 check if this stage is src
    // 2.1.1 set target[i] to dest
    // 2.1.2 set dest and src to -1
    outType& target = d*(c_comp_eq(33) + 3*c_mux(32));      // first iteration src cannot equal to stage as it is -1
    // 2.2 initialize not_full to false
    // 2.3 for each element in this bucket
    // 2.3.1 check if there is an empty slot (update not_full with OR dummy)
    target += (d*(B-1)+s-1)*c_lin_gate();
    // 2.3.2 check if (dest is not set AND bucket is not full OR target[i] is set) AND deepest[i] is set
    // 2.3.2.1 set src to deepest[i]
    // 2.3.2.2 set dest to i
    target += (d+1)*(3*c_comp_eq(33) + 3*c_lin_gate() + 2*c_mux(32)) - 2*(c_comp_eq(33) + c_lin_gate());        // first iteration dest and target[i] are constant
    //std::cout << "target: " << target << std::endl;
    return target;
}

// 3. prepare target
outType& CORAMOptRek::prepare_target() {
    // 1 initialize dest and src to -1
    // 2 for each stage (from leaf to stash)
    // 2.1 check if this stage is src
    // 2.1.1 set target[i] to dest
    // 2.1.2 set dest and src to -1
    outType& target = d*(c_comp_eq(myLog2(d+2)) + 3*c_mux(myLog2(d+2)));  // first iteration src cannot equal to stage as it is -1
    // 2.2 initialize not_full to false
    // 2.3 for each element in this bucket
    // 2.3.1 check if there is an empty slot (update not_full with OR dummy)
    target += (d*(B-1)+s-1)*c_lin_gate();
    // 2.3.2 check if (dest is not set AND bucket is not full OR target[i] is set) AND deepest[i] is set
    // 2.3.2.1 set src to deepest[i]
    // 2.3.2.2 set dest to i
    target += (d+1)*(3*c_comp_eq(myLog2(d+2)) + 3*c_lin_gate() + 2*c_mux(myLog2(d+2))) - 2*(c_comp_eq(myLog2(d+2)) + 2*c_lin_gate());    // first iteration dest and target[i] are constant
    //std::cout << "target: " << target << std::endl;
    return target;
}

// 4. evict once
outType& CORAMOrigRek::evict_once() {
    // 1 initialize dest to -1 and to_write_depth as well as hold_depth to 0
    // 2 for each stage
    // 2.1 set to_write->is_dummy to true
    // 2.2 check if hold->is_dummy is false AND i is dest
    // 2.2.1 copy block from hold to to_write
    // 2.2.2 set hold->is_dummy to true
    // 2.2.3 set to_write_depth to hold_depth
    // 2.2.4 set dest to -1
    outType& evictOnce = d*(c_lin_gate() + c_comp_eq(33) + c_mux(b+32+1+d) + c_lin_gate() + 2*c_mux(32));
    // 2.3 check if target[i] is set
    // 2.3.1 for each element of the bucket
    // 2.3.1.1 check if deepest_index[i] is this element
    // 2.3.1.1.1 copy block from the path to hold
    // 2.3.1.1.2 set is_dummy of the element on the path to true
    // 2.3.1.1.3 set dest to target[i]
    // 2.3.1.1.4 set hold_depth to deepest_depth[i]
    evictOnce += (d+1)*c_comp_eq(33) + (B*d+s)*(c_comp_eq(33) + c_mux(b+32+1+d) + c_mux(1) + 2*c_mux(32));
    // 2.4 set added to false
    // 2.5 check if to_write->is_dummy is false
    // 2.5.1 for each element of the bucket
    // 2.5.1.1 check if the element on path is a dummy AND added is false
    // 2.5.1.1.1 copy block from to_write to the path
    // 2.5.1.1.2 set added to true
    // 2.5.1.1.3 check if to_write_depth is greater than deepest_depth[i]
    // 2.5.1.1.3.1 set deepest_depth[i] to to_write_depth
    // 2.5.1.1.3.2 set deepest_index[i] to index of element within bucket
    evictOnce += (B*d+s)*(c_lin_gate() + c_mux(b+32+1+d) + c_mux(1) + 2*c_mux(32)) + B*d*(c_comp_mag(32));    // to write_depth not set for stash
    //std::cout << "evictOnce: " << evictOnce << std::endl;
    return evictOnce;
}

outType& CORAMOptRek::evict_once() {
    // 1 initialize dest to -1 and to_write_depth as well as hold_depth to 0
    // 2 for each stage
    // 2.1 set to_write->is_dummy to true
    // 2.2 check if hold->is_dummy is false AND i is dest
    // 2.2.1 copy block from hold to to_write
    // 2.2.2 set hold->is_dummy to true
    // 2.2.3 set to_write_depth to hold_depth
    // 2.2.4 set dest to -1
    outType& evictOnce = d*(c_lin_gate() + c_comp_eq(myLog2(d+2)) + c_mux(b+2*myLog2(m)+1) + c_lin_gate() + 2*c_mux(myLog2(d+2)));
    // 2.3 check if target[i] is set
    // 2.3.1 for each element of the bucket
    // 2.3.1.1 check if deepest_index[i] is this element
    // 2.3.1.1.1 copy block from the path to hold
    // 2.3.1.1.2 set is_dummy of the element on the path to true
    // 2.3.1.1.3 set dest to target[i]
    // 2.3.1.1.4 set hold_depth to deepest_depth[i]
    evictOnce += (d+1)*c_comp_eq(myLog2(d+2)) + B*d*(c_comp_eq(myLog2(B)) + c_mux(myLog2(B)));
    evictOnce += s*(c_comp_eq(myLog2(s)) + c_mux(myLog2(s))) + (B*d+s)*(c_mux(b+2*myLog2(m)+1) + c_mux(1) + c_mux(myLog2(d+2)));
    // 2.4 set added to false
    // 2.5 check if to_write->is_dummy is false
    // 2.5.1 for each element of the bucket
    // 2.5.1.1 check if the element on path is a dummy AND added is false
    // 2.5.1.1.1 copy block from to_write to the path
    // 2.5.1.1.2 set added to true
    // 2.5.1.1.3 check if to_write_depth is greater than deepest_depth[i]
    // 2.5.1.1.3.1 set deepest_depth[i] to to_write_depth
    // 2.5.1.1.3.2 set deepest_index[i] to index of element within bucket
    evictOnce += (B*d+s)*(c_lin_gate() + c_mux(b+2*myLog2(m)+1) + c_mux(1) + 2*c_mux(myLog2(d+2))) + B*d*c_comp_mag(myLog2(d+2));  // to write_depth not set for stash
    //std::cout << "evictOnce: " << evictOnce << std::endl;
    return evictOnce;
}

outType& CORAMOrigRek::add_and_evict() {
    // 1. instantiate a new block
    // 2. add block to stash
    outType& evict = 2*(prepare() + prepare_deepest() + prepare_target() + evict_once());
    return c_mux(b+32+1+d) + stash_add() + evict;
}

outType& CORAMOptRek::add_and_evict() {
    // 1. instantiate a new block
    // 2. add block to stash
    // 3. evict
    outType& evict = 2*(prepare() + prepare_deepest() + prepare_target() + evict_once());
    return stash_add() + evict;
}

outType& CORAMOrigRek::stash_add() {
    return 2*(s-1)*c_lin_gate() + s*(c_mux(b+32) + 2*c_lin_gate());
}

outType& CORAMOptRek::stash_add() {
    return 2*(s-1)*c_lin_gate() + s*c_mux(b+2*myLog2(m)+1);
}

outType& CORAMOrigRek::read_and_remove() {
    // 1. reveal position label
    // 2. collect all elements on path
    // 3. initialize results block
    // 4. bucket RAR TODO: hier mein Vorteil: leaf und vid irrelevant!
    outType& out = (B*d+s)*(c_comp_eq(33) + c_lin_gate() + c_mux(b+32+2+d));
    // 5. copy resulting block into result
    out += c_mux(32);
    return out;
}

outType& CORAMOptRek::read_and_remove() {
    // 1. reveal position label
    // 2. collect all elements on path
    // 3. initialize results block
    // 4. bucket RAR
    outType& out = (B*d+s)*(c_comp_eq(myLog2(m)) + 2*c_lin_gate() + c_mux(b));
    // 5. copy resulting block into result
    return out;
}

outType& CORAMOrigRek::access() {
    // get random needs d gates
    return read_and_remove() + extract() + d*c_lin_gate() + pack() + add_and_evict();
}

outType& CORAMOptRek::access() {
    // get random needs d gates
    return read_and_remove() + extract() + pack() + add_and_evict();
}

outType& CORAMOrigRek::extract() {
    return 8*(c_comp_eq(3+1) + c_mux(8, d+3));
}

outType& CORAMOrigRek::pack() {
    return 8*(c_comp_eq(3+1) + c_mux(d+3));
}

outType& CORAMOptRek::extract() {
    return c_mux(8, d+3);
}

outType& CORAMOptRek::pack() {
    return 8*c_mux(d+3) + 7*c_lin_gate();
}

outType& CORAMOrig::access() {
    // 1. check bounds
    // 2. read map
    // 3. Read and Remove
    // 4. Add
    outType& bounds = (3040-myLog2(m)*96)*c_lin_gate() + (32-myLog2(m)-1)*c_lin_gate();
    return lumu(1) + maps[0]->read_and_remove() + maps[0]->add_and_evict() + bounds;
}

outType& CORAMOrig::lumu(uint16_t stage) {
    if(stage == levels) {
        // 1. read element from last map (Linear Scan)
        // 2. get a new random leaf id
        // 3. write updated element into map
        int bool_to_int[] = {0, 0, 0, 0, 0, 58, 82, 110, 142, 82, 110, 142, 82, 110, 142};
        return ls_read(baseM, baseB) + ls_write(baseM, baseB) +  bool_to_int[myLog2(m)]*c_lin_gate();
    }
    else {
        // 1. calculate new index by using shifting
        // 2. update map below
        // 3. do read and remove on current map
        // 4. do a Linear Scan on the received element to extract correct mapping
        // 5. get a new random leaf id
        // 6. put new element back into the block
        // 7. do add and evict on current map
        int bool_to_int[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 178, 218, 262, 310, 362, 418};
        return lumu((uint16_t) (stage+1)) + maps[stage]->access() + bool_to_int[myLog2(m)]*c_lin_gate();
    }
}

outType& CORAMOrig::ls_read(uint64_t m, uint64_t b) {
    return (m-1)*c_comp_eq(8+1) + m*c_mux(b);        // we can have at most 2⁸ elements in this map!
}

outType& CORAMOrig::ls_write(uint64_t m, uint64_t b) {
    return m*(c_comp_eq(8+1) + c_mux(b));
}

outType& CORAMOpt::access() {
    // 2. Read and Remove
    // 3. Add
    return lumu(1) + maps[0]->read_and_remove() + maps[0]->add_and_evict();
}

outType& CORAMOpt::lumu(uint16_t stage) {
    if(stage == levels) {
        // 1. read element from last map (Linear Scan)
        // 2. get a new random leaf id
        // 3. write updated element into map
        return ls_read(baseM, baseB) + ls_write(baseM, baseB);
    }
    else {
        // 1. calculate new index by using shifting
        // 2. update map below
        // 3. do read and remove on current map
        // 4. do a Linear Scan on the received element to extract correct mapping
        // 5. get a new random leaf id
        // 6. put new element back into the block
        // 7. do add and evict on current map
        return lumu((uint16_t) (stage+1)) + maps[stage]->access();
    }
}

outType& CORAMOpt::ls_read(uint64_t m, uint64_t b) {
    return c_mux(m, b);
}

outType& CORAMOpt::ls_write(uint64_t m, uint64_t b) {
    return m*c_mux(b) + (m-1)*c_lin_gate();
}