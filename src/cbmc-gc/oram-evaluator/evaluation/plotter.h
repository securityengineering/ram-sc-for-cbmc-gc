//
// Created by weber on 27.07.2017.
//

#ifndef ORAMEVALUATOR_PLOTTER_H
#define ORAMEVALUATOR_PLOTTER_H

#include <list>
#include <utility>
#include "evaluator.h"
#include "csvFileWriter.h"

class Plotter : public Evaluator {
    std::string pathname;
public:
    Plotter() : Evaluator(), pathname("") { }
private:
    void plot(uint64_t noAcc, bool values, uint16_t d, uint64_t b, CSVFileWriter* writer, funcTypes func, double msFactor=1000);

public:
    void plot_acc_elements(evalParam dParam, uint64_t b, const std::string& filename);
    void plot_accesses(evalParam dParam, uint64_t (*accFunc)(uint16_t d), uint64_t b, const std::string& filename);
    void plot_accesses(uint16_t d, uint64_t b, const std::string& filename);
    void plot_accesses2(uint16_t d, uint64_t b, const std::string& filename);
    void plot_accesses3(uint16_t d, uint64_t b, const std::string& filename);
    void plot_elements(uint64_t noAcc, bool values, evalParam dParam, uint64_t b, const std::string& filename);

    void plot_acc_bitwidth(uint64_t m, evalParam bParam, const std::string& filename);
    void plot_bitwidth(uint64_t noAcc, bool values, uint64_t m, evalParam bParam, const std::string& filename);

    inline void setPath(std::string path) {
        pathname = std::move(path);
    }
};

#endif //ORAMEVALUATOR_PLOTTER_H
