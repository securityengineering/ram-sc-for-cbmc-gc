//
// Created by anon on 02.09.17.
//

#ifndef CBMC_GC_ALINA_OUTPUT_H
#define CBMC_GC_ALINA_OUTPUT_H

#include <iostream>
#include "expr.h"

void print_always(const std::string& s);
void print_always(const std::string& s, const exprt& e, bool print_fully);

void print_to_index_expr(const exprt& e);
void print_to_array_expr(const exprt& e);
void print_to_member_expr(const exprt& e);
void print_to_constant_expr(const exprt& e);

void print_misc(const std::string& s);
void print_misc(const std::string& s, const exprt& e, bool print_fully);

void print_step_1(const std::string& s);
void print_step_1(const std::string& s, const exprt& e, bool print_fully);

void print_step_2(const std::string &s);
void print_step_2(const std::string &s, const exprt &e, bool print_fully);

void print_step_3(const std::string &s);
void print_step_3(const std::string &s, const exprt &e, bool print_fully);

void print_step_4(const std::string &s);
void print_step_4(const std::string &s, const exprt &e, bool print_fully);

void print_step_5(const std::string &s);
void print_step_5(const std::string &s, const exprt &e, bool print_fully);

void print_step_6(const std::string &s);
void print_step_6(const std::string &s, const exprt &e, bool print_fully);

void print_step_7(const std::string &s);
void print_step_7(const std::string &s, const exprt &e, bool print_fully);

#endif //CBMC_GC_ALINA_OUTPUT_H
