#include <inttypes.h>

typedef struct {
	int8_t m[3][3];
} Matrix3x3;

Matrix3x3 mpc_main(Matrix3x3 INPUT_A, Matrix3x3 INPUT_B) {
	Matrix3x3 out;

	for(uint8_t row = 0; row < 3; ++row) {
		for(uint8_t col = 0; col < 3; ++col) {
			int8_t acc = 0;
			for(uint8_t i = 0; i < 3; ++i)
				acc += INPUT_A.m[row][i] * INPUT_B.m[i][col];

			out.m[row][col] = acc;
		}
	}

	return out;
}

