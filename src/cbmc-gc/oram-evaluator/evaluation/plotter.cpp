//
// Created by weber on 27.07.2017.
//

#include "plotter.h"

void Plotter::plot(uint64_t noAcc, bool values, uint16_t d, uint64_t b, CSVFileWriter* writer, funcTypes func, double msFactor) {
    auto m = (uint64_t) pow(2, d);

    //writer->addOutType(find_LinearScan(noAcc, 0, m, b));
    writer->addOutType(find_LinearScan(0, noAcc, m, b), msFactor);
    //writer->addOutType(find_best_BT(noAcc, values, m, b, StandardBTB(d), func.btF));
    //writer->addOutType(find_best_Path(noAcc, values, m, b, "Path ORAM", StandardPathB(d), StandardPathS(), func.pathF));
    //writer->addOutType(find_best_Path(noAcc, values, m, b, "Path-SC ORAM", StandardPathSCB(d), StandardPathS(), func.pathSCF));
    //writer->addOutType(find_best_Path(noAcc, values, m, b, "SCORAM", StandardScoramB(d), StandardScoramS(d), func.SCORAMF));

    writer->addOutType(find_best_Path(noAcc, values, m, b, "Circuit ORAM", StandardCoramB(d), StandardCoramS(), func.CORAMF), msFactor);
    std::cout << "Circuit ORAM done" << std::endl;

    if(noAcc == 1) {
        auto sqrtAcc = (uint32_t) ceil(sqrt(m * d - m + 1)) + 1;
        //std::cout << "SQRT old: " << sqrtAcc << " accesses: ";
        writer->addOutType(divideWR(*(find_best_OSQR(sqrtAcc, values, m, b, StandardSqrT(d), func.OSQRF).out), sqrtAcc), msFactor);
				std::cout << "SQRT done" << std::endl;
        //std::cout << "SQRT new:  use worst-case scenario"<< std::endl;
        //writer->addOutType(find_best_OSQR(0, values, m, b, StandardSqrT(d), acc_OSQR_slow_worst));
        auto c = (uint16_t) (b > 128? 1 : floor(128/b));
        auto floramAcc = (uint16_t) ceil(sqrt(m/c)/8);
        std::cout << "Floram: " << floramAcc << " accesses" << std::endl;
        writer->addOutType(divideWR(acc_FLORAM_CPRG_slow(floramAcc, values, m, b, UINT16_MAX, UINT16_MAX), floramAcc), msFactor);
        std::cout << "FLORAM CPRG done" << std::endl;

        writer->addOutType(divideWR(acc_FLORAM(floramAcc, values, m, b, UINT16_MAX, UINT16_MAX), floramAcc), msFactor);
        std::cout << "FLORAM done" << std::endl;
    }
    else {
        std::cout << "accesses: " << noAcc << std::endl;
        writer->addOutType(find_best_OSQR(noAcc, values, m, b, StandardSqrT(d), func.OSQRF), msFactor);
        std::cout << "SQRT done" << std::endl;
        writer->addOutType(acc_FLORAM_CPRG_slow(noAcc, values, m, b, UINT16_MAX, UINT16_MAX), msFactor);      // TODO hier den funktionsdingens einfügen
        std::cout << "FLORAM CPRG done" << std::endl;
        writer->addOutType(acc_FLORAM(noAcc, values, m, b, UINT16_MAX, UINT16_MAX), msFactor);
        std::cout << "FLORAM done" << std::endl;
    }
    std::cout << "Plotted d = " << d << std::endl;
}

void Plotter::plot_acc_elements(evalParam dParam, uint64_t b, const std::string& filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename, pathname);
   // writer->addHeader("d, Linear Scan Read, Linear Scan Write, Binary Tree ORAM, Path ORAM, Path-SC, SCORAM, Circuit ORAM, Optimized SQR ORAM, FLORAM CPRG");
    writer->addHeader("d, TLS, CORAM, SQR, FLORAM CPRG, FLORAM");
    clock_t start = clock();

    for(uint16_t d = dParam.min; d <= dParam.max; d = d*dParam.step_m + dParam.step_p) {
        writer->addLine(d);
        plot(1, false, d, b, writer, acc_slow_func);                // TODO: umstellen auf fast
    }

    writer->writeFiles();
    delete writer;
    float elapsed = (float)(clock() - start) / CLOCKS_PER_SEC;
    std::cout << "Plotted all - time needed: " << elapsed << std::endl;
}

void Plotter::plot_accesses(evalParam dParam, uint64_t (*accFunc)(uint16_t d), uint64_t b, const std::string& filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename, pathname);
    writer->addHeader("d, TLS, CORAM, SQR, FLORAM CPRG, FLORAM");

    for(uint16_t d = dParam.min; d <= dParam.max; d = d*dParam.step_m + dParam.step_p) {
        writer->addLine(d);
        plot(accFunc(d), false, d, b, writer, acc_slow_func);                // TODO: umstellen auf fast
    }

    writer->writeFiles();
    delete writer;
}

// Optimized for big numbers, e.g. m=2^32,
void Plotter::plot_accesses2(uint16_t d, uint64_t b, const std::string &filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename, pathname);
    writer->addHeader("noAcc, TLS, CORAM, SQR, FLORAM CPRG, FLORAM");

		unsigned num_intervals=30;

    uint64_t accP[num_intervals];
    accP[0] = 1; // 2^0
    //accP[1] = d; 
    
    unsigned max_shift = 3*d/2;
    unsigned shift = ceil(max_shift / num_intervals); 
    
    for(uint8_t i = 1; i < num_intervals; i++) {
			accP[i] = (uint64_t) 1 << (shift * i);
		}

    for (unsigned long i : accP) {
        writer->addLine(myLog2(i));
        plot(i, true, d, b, writer, slow_func, 1000*3600*24); // result in days
    }
    writer->writeFiles();
    delete writer;
}

// Hardcoded, plots from n=1 to n=61
void Plotter::plot_accesses3(uint16_t d, uint64_t b, const std::string &filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename, pathname);
    writer->addHeader("noAcc, TLS, CORAM, SQR, FLORAM CPRG, FLORAM");

		unsigned num_intervals=30;
    uint64_t accP[num_intervals];
    accP[0] = 2; // 2^0
    //accP[1] = d; 
    writer->addLine(2);
    plot(2, true, d, b, writer, slow_func);
    for(uint8_t i = 1; i < num_intervals; i++) {
			accP[i] = accP[i-1]+2;
			writer->addLine(accP[i]);
			plot(accP[i], true, d, b, writer, slow_func);
		}

    writer->writeFiles();
    delete writer;
}


void Plotter::plot_accesses(uint16_t d, uint64_t b, const std::string &filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename, pathname);
    writer->addHeader("noAcc, TLS, CORAM, SQR, FLORAM CPRG, FLORAM");

    uint64_t accP[10];
    // first data point: d
    accP[0] = d;
    // fourth data point: m
    accP[3] = getM(d);
    // seventh data point: m logm
    accP[6] = getMlogM(d);
    // last data point: m^2
    accP[9] = getMSquare(d);

    for(uint8_t i = 1; i < 10;) {
        accP[i] = accP[i-1] + (accP[i+2] - accP[i-1]) / 3;
        accP[i+1] = accP[i-1] + 2*(accP[i+2] - accP[i-1]) / 3;
        i += 3;
    }
    for (unsigned long i : accP) {
        writer->addLine(i);
        plot(i, true, d, b, writer, slow_func);
    }
    writer->writeFiles();
    delete writer;
}

void Plotter::plot_elements(uint64_t noAcc, bool values, evalParam dParam, uint64_t b, const std::string& filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename);
    //writer->addHeader("d, Linear Scan Read, Linear Scan Write, Binary Tree ORAM, Path ORAM, Path-SC, SCORAM, Circuit ORAM, Optimized SQR ORAM, FLORAM CPRG");
    writer->addHeader("d, TLS, CORAM, SQR, FLORAM, FLORAM CPRG");
    clock_t start = clock();

    for(uint16_t d = dParam.min; d <= dParam.max; d = d*dParam.step_m + dParam.step_p) {
        writer->addLine(d);
        plot(noAcc, values, d, b, writer, slow_func);                // TODO: umstellen auf fast
    }

    writer->writeFiles();
    delete writer;
    float elapsed = (float)(clock() - start) / CLOCKS_PER_SEC;
    std::cout << "Plotted all - time needed: " << elapsed << std::endl;
}

void Plotter::plot_acc_bitwidth(uint64_t m, evalParam bParam, const std::string& filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename);
    writer->addHeader("b, Linear Scan Read, Linear Scan Write, Binary Tree ORAM, Path ORAM, Path-SC, SCORAM, Circuit ORAM, Optimized SQR ORAM, FLORAM CPRG");
    clock_t start = clock();
    for(uint64_t b = bParam.min; b <= bParam.max; b = b*bParam.step_m + bParam.step_p) {
        writer->addLine(b);
        uint16_t d = myLog2(m);
        plot(1, false, d, b, writer, acc_slow_func);                // TODO: umstellen auf fast
    }
    writer->writeFiles();
    delete writer;
    float elapsed = (float)(clock() - start) / CLOCKS_PER_SEC;
    std::cout << "Plotted all - time needed: " << elapsed << std::endl;
}


void Plotter::plot_bitwidth(uint64_t noAcc, bool values, uint64_t m, evalParam bParam, const std::string& filename) {
    CSVFileWriter* writer = new CSVFileWriter(filename);
    writer->addHeader("b, Linear Scan Read, Linear Scan Write, Binary Tree ORAM, Path ORAM, Path-SC, SCORAM, Circuit ORAM, Optimized SQR ORAM, FLORAM CPRG");
    clock_t start = clock();
    for(uint64_t b = bParam.min; b <= bParam.max; b = b*bParam.step_m + bParam.step_p) {
        writer->addLine(b);
        uint16_t d = myLog2(m);
        plot(noAcc, values, d, b, writer, slow_func);                // TODO: umstellen auf fast
    }
    writer->writeFiles();
    delete writer;
    float elapsed = (float)(clock() - start) / CLOCKS_PER_SEC;
    std::cout << "Plotted all - time needed: " << elapsed << std::endl;
}
