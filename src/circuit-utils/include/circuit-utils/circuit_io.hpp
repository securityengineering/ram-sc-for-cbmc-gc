#include "circuit.hpp"


namespace circ {

// Various circuit file formats
//==================================================================================================
Circuit read_cbmc_circuit(std::string const &path);

Circuit read_bristol_circuit(std::ifstream &file);
Circuit read_bristol_circuit(std::string const &filepath);
void write_bristol_circuit(Circuit const &circuit, std::ofstream &file);
void write_bristol_circuit(Circuit const &circuit, std::string const &filepath);

void write_shdl_circuit(Circuit const &circuit, std::string const &filepath);

void write_scd_circuit(Circuit const &circuit, std::string const &filepath);


//------------------------------------------------------------------------------
enum class CircuitFileFormat
{
	cbmc_gc,
	bristol,
	shdl,
	scd,
};

inline char const* cstr(CircuitFileFormat f)
{
	switch(f)
	{
		case CircuitFileFormat::cbmc_gc: return "CBMC-GC";
		case CircuitFileFormat::bristol: return "Bristol";
		case CircuitFileFormat::shdl: return "SHDL";
		case CircuitFileFormat::scd: return "SCD";
	}
}

inline Circuit read_circuit(std::string const &path, CircuitFileFormat fmt)
{
	Circuit circuit;
	switch(fmt)
	{
		case CircuitFileFormat::cbmc_gc: circuit = read_cbmc_circuit(path); break;
		case CircuitFileFormat::bristol: circuit = read_bristol_circuit(path); break;
		default:
			throw std::runtime_error{std::string{"Reading from "} + cstr(fmt) + " file not yet supported"};
	}

	validate(circuit);
	return circuit;
}


// *.dot export
//==================================================================================================
inline std::string elem_id_to_string(ElementID id)
{
	switch(id.kind())
	{
		case ElementID::Kind::gate: return std::to_string(id.id());
		case ElementID::Kind::const_one: return "const_one";
		case ElementID::Kind::input: return "in_" + std::to_string(id.id());
		case ElementID::Kind::output: return "out_" + std::to_string(id.id());
		default: throw std::logic_error{"Invalid element kind: " + std::to_string((uint64_t)id.kind())};
	}
}

inline void to_dot(std::ostream &os, Circuit const &circ)
{
	os << "digraph {\n";

	for(size_t id = 0; id < circ.gates.size(); ++id)
	{
		Gate const &gate = circ.gates[id];
		os << '\t' << id << " [label=\"[" << id << "] " << to_string(gate.kind) << "\"];\n";
	}

	for(size_t id = 0; id < circ.inputs.size(); ++id)
	{
		os << '\t' << elem_id_to_string(InputID{id})
		   << " [label=\"in_" << to_string(circ.inputs[id]) << "_" << id << "\"];\n";
	}

	for(size_t id = 0; id < circ.gates.size(); ++id)
	{
		Gate const &gate = circ.gates[id];
		for(int i = 0; i < gate.num_fanins; ++i)
			os << '\t' << elem_id_to_string(gate.fanins[i]) << " -> " << elem_id_to_string(GateID{id}) << ";\n";
	}

	for(size_t id = 0; id < circ.outputs.size(); ++id)
		os << '\t' << elem_id_to_string(*circ.outputs[id]) << " -> " << elem_id_to_string(OutputID{id}) << ";\n";

	os << "}\n";
}

inline void to_dot(std::ostream &&os, Circuit const &circ)
{
	to_dot(os, circ);
}


//==================================================================================================
void to_c_code(std::ostream &os, Circuit const &circ, std::string const &func_name);

inline void to_c_code(std::ostream &&os, Circuit const &circ, std::string const &func_name)
{
	to_c_code(os, circ, func_name);
}


}
