//
// Created by anon on 17.09.17.
//

#ifndef CBMC_GC_ORAM_GOTO_SYMEXT_H
#define CBMC_GC_ORAM_GOTO_SYMEXT_H

#include <goto-symex/goto_symex.h>
#include <cbmc/symex_bmc.h>

struct oramt {
    std::string identifier;
    mp_integer noElements;
    mp_integer elementSize;
    bool initValues = false;

    uint32_t secretRead = 0;
    uint32_t secretWrite = 0;
    uint32_t constRead = 0;
    uint32_t constWrite = 0;

    oramt(std::string identifier, mp_integer noElements, mp_integer elementSize)
            : identifier(identifier), noElements(noElements), elementSize(elementSize) { }
};

typedef std::unordered_map<std::string, oramt*> oramMapping;
typedef std::unordered_map<const exprt, int, irep_hash> op_cachet;      // TODO: fix me (why the fuck is a set not possible???)

class oram_goto_symext : virtual public goto_symext {
private:
    oramMapping& oramMap;
    op_cachet op_cache;
    uint8_t array_rec_level = 0;
public:
    oram_goto_symext(const namespacet& _ns, symbol_tablet& _new_symbol_table, symex_targett& _target, oramMapping& oramMap)
            : goto_symext(_ns, _new_symbol_table, _target), oramMap(oramMap) {}

    inline ~oram_goto_symext() {
        op_cache.clear();
        oramMap.clear();
    }

    static std::string filter_identifier(irep_idt identifier);
    static std::string get_identifier_string(irep_idt identifier, uint8_t array_rec_level);

    std::string complete_member_expression(member_exprt& member);
    std::string complete_symbol_expression(symbol_exprt& symbol);
    void complete_array_expression(array_exprt& array, std::string identifier);
    std::string complete_expressions(exprt& expr);

    irep_idt handle_declaration(const array_typet& array, irep_idt identifier);
    irep_idt handle_declaration(const array_typet& array, std::string identifier);
    irep_idt handle_member_declaration(member_exprt& member);

    void handle_initialization(const symbol_exprt& symbol, const array_exprt& array);
    void handle_initialization(const array_exprt& array, std::string identifier);
    void handle_member_initialization(exprt& expr, std::string identifier);
    void handle_member_initialization(typet& type, std::string identifier);

    bool filter_index_op(goto_symext::statet& state);
    index_exprt& get_innermost_index(index_exprt& index);
    void handle_access(exprt& expr, exprt& index, goto_symext::statet &state, irep_idt identifier, bool write);
    void handle_index(index_exprt& index, goto_symext::statet &state, bool write);

    void symex_decl(statet &state, const symbol_exprt &expr) override;
    void symex_assign_rec(statet &state, const code_assignt &code) override;
    void dereference_rec(exprt &expr, statet &state, guardt &guard, const bool write) override;
};

class cbmc_ramsc_goto_symext : public symex_bmct, public oram_goto_symext {
public:
    cbmc_ramsc_goto_symext(const namespacet &_ns, symbol_tablet &_new_symbol_table, symex_targett &_target, oramMapping& oramMap)
            : goto_symext(_ns, _new_symbol_table, _target),
              symex_bmct(_ns, _new_symbol_table, _target),
              oram_goto_symext(_ns, _new_symbol_table, _target, oramMap) { }

    inline cbmc_ramsc_goto_symext(const goto_symext& ref, oramMapping& oramMap)
            : goto_symext(ref),
              symex_bmct(goto_symext::ns, goto_symext::new_symbol_table, goto_symext::target),
              oram_goto_symext(goto_symext::ns, goto_symext::new_symbol_table, goto_symext::target, oramMap) { }
};

inline std::ostream& operator<<(std::ostream& ostr, const oramt& o) {
    ostr << "identifier: " << o.identifier << "\n";
    ostr << "noElements: " << o.noElements << "\n";
    ostr << "elementSize: " << o.elementSize << "\n";
    ostr << "initValues: " << (o.initValues? "true" : "false") << "\n";
    ostr << "secretRead: " << o.secretRead << "\n";
    ostr << "secretWrite: " << o.secretWrite << "\n";
    ostr << "constRead: " << o.constRead << "\n";
    ostr << "constWrite: " << o.constWrite << "\n";
    return ostr;
}

#endif //CBMC_GC_ORAM_GOTO_SYMEXT_H