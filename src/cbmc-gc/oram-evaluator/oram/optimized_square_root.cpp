//
// Created by weber on 20.08.2017.
//

#include "optimized_square_root.h"
#include <typeinfo>

outType& OSquareRoot::c_init(bool values) {
    if(values) {
        // 1. permute memory (reinitialize map done automatically)
        outType& out = c_permute();

        // 2. reset used bits of permuted memory
        // 3. reset t
        t = 0;
        return out;
    }

    return 0*c_lin_gate();
}

outType& OSquareRoot::c_LUMU() {
    // assume index to read is only known in SC

    // case Square Root ORAM
    // 1. read block at "random" index from map (block is written to stash of map automatically)
    // 2. unpack block and extract index
    // 3. return new "random" index for use on permuted memory to generator party (Yao to plaintext)
    if(typeid(*map) == typeid(OSquareRoot))
        return addWR(TrivialLinearScan::c_read(c, myLog2(m)) + map->c_acc(c*myLog2(m)), c_transmit(myLog2(m)));

    // case Trivial Linear Scan:
    // 1. read index to use on permuted memory from map (uses found bit)
    // 2. return new "random" index for use on permuted memory to generator party (Yao to plaintext)
    return addWR(map->c_acc(myLog2(m)), c_transmit(myLog2(m)));
}

outType& OSquareRoot::c_acc(uint64_t b) {
    // assuming index to read is known in SC
    outType& out = 0*c_lin_gate();          // TODO: fix this

    // 1. scan stash up to position t
    if (t != 0)
        out += c_scan_stash();

    // 2. look up index to use for permuted memory by using the map and the found bit
    out = out.addWR(c_LUMU());

    // 3. read element from permuted memory (we always do both - read an write - but write is to stash)
    //out = out.addWR(c_B2Y(1, b));
    // Todo change to soldering
    out = out.addWR(c_transmit_in_same_round(myLog2(m)));
    
    // 3.1 set Used bit of read element

    // 4. update stash
    // 4.1 if not found, write stash[0] to stash[t] and shuff[i'] to stash[0]
    // 4.2 if found, write shuff[i'] to stash[t]
    // -> Element to write is always at position stash[0] - only needs b bit as index is known
    out += c_mux(bb) + c_mux(b);

    // 5. write data to stash[0]
    t++;
    return out;
}

outType& OSquareRoot::c_scan_stash() {
    // for each element in the stash till position t
    // 1. check if this is the correct index
    // 2. connect the output of the comparator with a condSwap between the current position and stash[0]
    // 3. update "found" boolean
    return t*c_comp_eq(myLog2(m)) + (t-1)*(c_condSwap(bb) + c_lin_gate());
}

outType& OSquareRoot::c_acc(uint64_t noAccess, uint64_t b) {
    outType& mod = 0*c_lin_gate();
    outType& full = 0*c_lin_gate();
    for(uint64_t i = 1; i <= noAccess && i <= T; i++) {
        full = full.addWR(c_acc(b));

        if(i == noAccess % T) // mod stores remainder
            mod = (outType) {full.gates, full.traffic, full.rounds};
    }

    //auto epochs = (uint64_t) (noAccess == T)? 1 : (noAccess-1) / T;
    auto epochs = (uint64_t) noAccess / T;
    outType& out = multiplyWR(epochs, addWR(full, c_refresh()));
    if(noAccess%T != 0) { // Not hte nicest code structure, but outType needs some form of copy
				outType& res = addWR(out, mod);
				return res;
		}
		return out;
}

outType& OSquareRoot::c_acc_worst(uint64_t b) {
    return c_acc(T+1, b);
}

outType& OSquareRoot::c_refresh() {
    // 1. update permuted memory with elements from stash
    // 2. reset t
    // 3. shuffle permuted memory
    // 4. update maps (done in permute() automatically)
    // 5. reset Used Bits
    outType& out = c_permute() + c_update();
    t = 0;

    return out;
}

outType& OSquareRoot::c_amortized(uint64_t noAcc, bool values) {
    return divideWR(addWR(c_init(values), c_acc(noAcc, b)), noAcc);
}

outType& OSquareRoot::c_permute() {
    // 1. convert Boolean-Shares of permuted memory to Yao-Shares
    //      if this is a map, this is not necessary, as the values to permute are equal to the permutation of the level above (already yao-share)
    outType& convert = isMap? 0*c_lin_gate() : c_B2Y(m, bb);
    // 2. create a new waksman network with a random permutation
    // 2.1 shuffle permuted memory according to control bits of party 1 (only half-gates)
    // 2.2 shuffle permuted memory according to control bits of party 2 (only traffic)
    outType& shuffle = addWR(convert, c_shuffle(m, bb, true) + c_shuffle(m, bb, false));
    //outType& shuffle = addWR(convert, c_shuffle(m, myLog2(m), true) + c_shuffle(m, myLog2(m), false));

    // 2.3 reconstruct permutation by obtaining the inversion
    // 2.3.1 party 1: get a new random permutation and set switches accordingly (pi^a)
    // 2.3.2 apply network -> pi^b = pi^(-1)*pi^a
    // 2.3.3 reveal result (pi^b) to party 2        // TODO: sollte das nicht umsonst sein?
    // 2.3.4 party 1: feed own permutation into SC (should be possible to do in parallel)
    outType& party1 = addWR(shuffle + c_shuffle(m, myLog2(m), true), c_transmit(m*myLog2(m)) + c_yaoShare(1, m*myLog2(m)-m+1));

    // 2.3.5 party 2: invert pi^b to get pi^(-b) and set switches of waksman accordingly
    // 2.3.6 apply network -> pi = pi^a * pi^(-b) (yao share of control bits should be possible in parallel)
    outType& party2 = party1 + c_shuffle(m, myLog2(m), false);

    // 3. update map
    // 4. convert permuted memory back to Boolean-Share
    return addWR(party2 + c_Y2B(m, bb), map->c_init(true));
}

outType& OSquareRoot::c_update() {
    // 1. for each element in the permuted memory
    // 1.1 if it is used
    // 1.1.1 copy next element from stash to this slot of the permuted memory
    // this is completely for free, as the used bits are public and the blocks can be copied locally  (Y2B is for free)
    return c_Y2B(t, bb);
}
