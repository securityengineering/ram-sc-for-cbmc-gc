//
// calculates circuit complexity fast by using minimized formulas
// Created by weber on 30.06.2017.
//
#include "fast_calculator.h"

#include <iostream>
#include <cmath>
#include "helper.h"
#include "primitives.h"

// TODO caution: corresponds to BT with dynamicbuckets false, while evaluation uses true!!!!
outType& c_acc_BT(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t c) {
    uint64_t newM = m;
    uint64_t newB = b;
    uint16_t r = 0;
    uint16_t d = myLog2(newM);
    auto* outT = new outType;
    *outT = {0, 0, 0};

    while (counter-- >= 0 && newM > 2) {
        d = myLog2(newM);

        uint64_t evictGates = (2*d-3)*(7*B-7+(3*B+1)*(newB+2*d+1))+B*(newB+2*d+1)+2*B-2;
        uint64_t evictTraffic = 256*(evictGates+(2*d-3)*(3*B+1)*(newB+2*d+1)) + 2*(d-1)*d-4;

        if(counter == 0)
            outT->traffic += 256*d;

        outT->gates += evictGates + (2*c*(d+1)-d) + d*B*(2+d+newB);
        outT->traffic += evictTraffic + 256*((2*c*(d+1)-d)) + 256*(d*B*(2+d+newB) + d*(B*(newB+1+d))+newB+2*d+1)+4*d;

        newM = (uint64_t) ceil((double) newM/c);
        newB = c*d;
        r++;
    }
    outT->rounds = (uint64_t) 6*r;
    outT->gates += (newM-1)*c*d + newM*c*d + 2*newM;
    outT->traffic += 256*((newM-1)*c*d + newM*c*d + 2*newM);
    return *outT;
}

void print_acc_BT(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t c) {
    outType& out = c_acc_BT(m, b, B, counter, c);
    std::cout << "BT: " << out << std::endl;
    delete &out;
}

outType& c_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t c) {
    std::cout << "c_BT: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_BT(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t c) {
    outType& out = c_BT(noAcc, values, m, b, B, counter, c);
    std::cout << "BT: " << out << std::endl;
    delete &out;
}

//       PATH          //
outType& c_acc_Path(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_acc_Path: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_acc_Path(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_acc_Path(m, b, B, counter, s, c);
    std::cout << "Path: " << out << std::endl;
    delete &out;
}

outType& c_Path(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_Path: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_Path(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_Path(noAcc, values, m, b, B, counter, s, c);
    std::cout << "Path: " << out << std::endl;
    delete &out;
}

//       PATH-SC          //
outType& c_acc_PathSC(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_acc_PathSC: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_acc_PathSC(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_acc_PathSC(m, b, B, counter, s, c);
    std::cout << "Path-SC: " << out << std::endl;
    delete &out;
}

outType& c_PathSC(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_PathSC: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_PathSC(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_PathSC(noAcc, values, m, b, B, counter, s, c);
    std::cout << "Path-SC: " << out << std::endl;
    delete &out;
}

//       SCORAM          //
outType& c_acc_SCORAM(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_acc_SCORAM: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_acc_SCORAM(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_acc_SCORAM(m, b, B, counter, s, c);
    std::cout << "SCORAM: " << out << std::endl;
    delete &out;
}

outType& c_SCORAM(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_SCORAM: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_SCORAM(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_SCORAM(noAcc, values, m, b, B, counter, s, c);
    std::cout << "SCORAM: " << out << std::endl;
    delete &out;
}

//       Circuit ORAM          //
outType& c_acc_CORAM(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_acc_CORAM: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_acc_CORAM(uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_acc_CORAM(m, b, B, counter, s, c);
    std::cout << "Circuit ORAM: " << out << std::endl;
    delete &out;
}

outType& c_CORAM(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    std::cout << "c_CORAM: TODO - implement me!" << std::endl;
    return *(new outType);
}

void print_CORAM(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t B, int16_t counter, uint16_t s, uint16_t c) {
    outType& out = c_CORAM(noAcc, values, m, b, B, counter, s, c);
    std::cout << "Circuit ORAM: " << out << std::endl;
    delete &out;
}

outType& c_acc_SQRORAM(uint64_t noAcc, uint64_t m, uint64_t b, uint16_t T, int16_t counter, uint16_t c) {
    std::cout << "c_acc_SQRORAM: TODO - implement me!" << std::endl;
    return *(new outType);
}

outType& c_SQRORAM(uint64_t noAcc, bool values, uint64_t m, uint64_t b, uint16_t T, int16_t counter, uint16_t c) {
    std::cout << "c_SQRORAM: TODO - implement me!" << std::endl;
    return *(new outType);
}

void test_fast_formulas() {
    print_acc_BT((uint64_t) pow(2, 30), 32, 120, 8, 16);
    print_acc_Path((uint64_t) pow(2, 30), 32, 4, 8, 89, 16);
    print_acc_PathSC((uint64_t) pow(2, 30), 32, 4, 8, 89, 16);
    print_acc_SCORAM((uint64_t) pow(2, 30), 32, 6, 8, 141, 16);
    print_acc_CORAM((uint64_t) pow(2, 30), 32, 4, 8, 89, 16);
}