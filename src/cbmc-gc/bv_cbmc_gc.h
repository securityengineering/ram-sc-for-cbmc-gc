#ifndef CBMC_GC_BV_CBMC_GC_DEFAULT_H
#define CBMC_GC_BV_CBMC_GC_DEFAULT_H

#include <cbmc/bv_cbmc.h>
#include "building_blocks.h"


enum class expr_optimizationt
{
  standard,
  low_depth,
};

// Responsible for converting expressions (exprt) to vectors of literals (bvt). The actual
// conversion to boolean formulas is done using the operation_convertert interface.
// 
// In the methods that are overwitten, often the only change is that operation_convertert is used
// instead of bvutils.
class bv_cbmc_gct : public bv_cbmct
{
public:
  bv_cbmc_gct(const namespacet &_ns, propt &_prop, building_blockst _conv, expr_optimizationt opt) :
    bv_cbmct(_ns, _prop),
    conv{_conv},
    optimization{opt} {}
  
  virtual ~bv_cbmc_gct() {}

  virtual bvt convert_add_sub(const exprt &expr) override;
  virtual bvt convert_unary_minus(const unary_exprt &expr) override;
  virtual bvt convert_mult(const exprt &expr) override;
  virtual bvt convert_div(const div_exprt &expr) override;
  virtual bvt convert_mod(const mod_exprt &expr) override;

  // changed by Alina
  inline bvt convert_array_initialization(const exprt &expr) override { return bv_cbmct::convert_array_initialization(expr); }

  inline void convert_with_array_constant(
          const array_typet &type, mp_integer size, mp_integer op1_value, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv)
          override { bv_cbmct::convert_write_constant(size, op1_value, op2_bv, prev_bv, next_bv); }

  inline void convert_write_constant(
          mp_integer size, mp_integer op1_value, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv)
          override { bv_cbmct::convert_write_constant(size, op1_value, op2_bv, prev_bv, next_bv); }

  inline void convert_with_array_dynamic(
          const array_typet &type, mp_integer size, const exprt &op1, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv)
          override { bv_cbmct::convert_write_dynamic(size, op1, op2_bv, prev_bv, next_bv); }

  inline void convert_write_dynamic(
          mp_integer size, const exprt &op1, const bvt &op2_bv, const bvt &prev_bv, bvt &next_bv)
          override { bv_cbmct::convert_write_dynamic(size, op1, op2_bv, prev_bv, next_bv); }

  inline bvt convert_member_declarate(const member_exprt &expr, const typet &struct_op_type, const bvt &struct_bv)
          override { return bv_cbmct::convert_member_declarate(expr, struct_op_type, struct_bv); }

  virtual bvt convert_index(const index_exprt &expr) override;
  virtual bvt convert_index_constant(exprt array, mp_integer index_value);
  virtual bvt convert_read_constant(exprt array, mp_integer index_value);
  virtual bvt convert_index_dynamic(const index_exprt expr, std::size_t width, mp_integer array_size, const bvt& array_bv);
  virtual bvt convert_read_dynamic(const exprt index, std::size_t width, mp_integer array_size, const bvt& array_bv);

  virtual bvt convert_shift(const binary_exprt &expr) override;
  virtual bvt convert_if(const if_exprt &expr) override;
  virtual literalt convert_equality(const equal_exprt &expr) override;
  virtual literalt convert_bv_rel(const exprt &expr) override;

  bvt convert_add_sub_lowdepth(const exprt &expr);

protected:
  building_blockst conv;
  expr_optimizationt optimization;
};

#endif
