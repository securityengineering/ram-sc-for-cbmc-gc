//
// Created by weber on 03.02.2018.

#include "Floram.h"


double interpolate5(double from, double to, int x) {
	assert(x>=0 && x < 5);
	if(x==0) {
		return from;
	} else if(x==1) {
		return (to-from)*5.0/20.0+from;
	} else if(x==2) {
		return (to-from)*9/20.0+from;
	}else if(x==3) {
		return (to-from)*13.5/20.0+from;
	}else if(x==4) {
		return (to-from)*18.0/20.0+from;
	}
}

outType& FloramOrig::c_init(bool values) {
    if(values) {
        return c_refresh();
    }
    std::cout << "Floram init without values is not implemented yet!" << std::endl;
    return 0*c_lin_gate();      // TODO
}

outType& FloramOrig::fss_getblockvector_with_callback(bool readOnly) {
    auto blocks = (uint64_t) (readOnly? (numB+128-1)/128 : numB);
    auto startlevel = (uint16_t) std::min(5, myLog2(blocks)-1);

    // 1. collect set of random blocks for the top level
    outType& yao = c_yaoShare((uint64_t) (1 << startlevel) + 1, 128);

    // 2. send identical blocks for all except the one branch we are interested in

    // 3. for min(32, numBlocks>>2)
    // 3.1 check if levelindex
    uint64_t eq = readOnly? 22 : 62;
    outType& loop = c_comp_eq((uint64_t) eq -(myLog2(blocks)-std::min(5, myLog2(blocks)-1)));

    // 3.1.1 copy block
    loop += c_mux(128);
    // 3.2 else
    // 3.2.1 copy block
    loop += c_mux(128);
    // 3.2.2 ensure root advice bits are opposite
    // 3.2.3 copy block
    loop += c_lin_gate() + c_mux(128);

    // 4. reveal keys (2*8*2*8 = 256 bit per level)
    loop += c_transmit(256);

    // 5. if endlevel > startlevel: fss traverselevels (21768 GA)
    outType& out = addWR(yao, (1<<startlevel)*loop);        // no multiplyWR - keys can be transmitted in parallel

    if(clog2(blocks) > startlevel)
        return out + fss_traverselevels(readOnly) + floram_scan_callback(readOnly);

    return out + floram_scan_callback(readOnly);
    // 6. write output (for free)
}

outType& FloramOrig::fss_traverselevels(bool readOnly) {
    auto blocks = (uint64_t) (readOnly? (numB+128-1)/128 : numB);
    auto its = (uint16_t) std::max(1, myLog2(blocks)-5);

    // 1. expand active blocks into two blocks
    outType& out = 2*c_SC_PRG();

    // 2. if levelindex == 0
    // 2.1 copy the branch to be kept
    out += 2*c_mux(128);
    // 2.2 xor blocks for the silenced branch
    out += c_mux(128) + 16*c_mux(8);
    // 3. else
    out += 4*c_mux(128);

    // 4. set control bits
    out += 4*(c_lin_gate()+c_mux(1));

    // 5. XOR live branches with Z if they will also be XORed by the offline component
    out += 2*16*c_mux(8);

    return its*out + (blockMul-1)*c_SC_PRG();
}

outType& FloramOrig::floram_scan_callback(bool readOnly) {
    if(t > 0) {
        // 1. check if first slot of stash was not already used (used bit)
        // 1.1 copy element of first slot to current slot
        // 1.2 set index of current slot to index of first slot
        outType& out = c_comp_eq(32) + blockMul*c_mux(128) + c_mux(32);

        // 2. set used bit of first slot
        // 3. for each other element in the stash
        // 3.1 check if index of slot is the searched one
        // 3.1.1 copy element from this slot to the first one
        // 3.1.2 set index of first slot to index of current slot
        // 3.1.3 set used bit of current slot (OR)
        // 3.1.4 set found to true (OR)
        return out + t*(c_comp_eq(64) + blockMul*c_mux(128)  + (readOnly? 0*c_lin_gate() : 2*c_mux(32) + c_lin_gate()));
    }
    return 0*c_lin_gate();
}

outType& FloramOrig::scanrom_read_with_bitvector() {
    // 1. ocFromSharedN
    outType& out = c_yaoShare(blockMul, 128);

    // 2. blockmultiple times
    // 2.1 PRG
    out += blockMul*c_SC_PRG();

    // 3. copy block
    return out + blockMul*c_mux(128);
}

outType& FloramOrig::scanwrom_write_with_blockvector() {
    // memblocksize times copy 8 bit
    return blockMul*c_mux(128) + 2*blockMul*c_transmit(128);
}

outType& FloramOrig::scanrom_read_with_blockvector() {
    // 1. calculate index within 128 bit block (byte) and index within byte (bit)
    outType& out = 2*2220*c_lin_gate();

    // 2. for each byte in the 128 bit block
    // 2.1 copy byte to temp block (for free)
    // 2.2 check if this is the correct byte AND check if it is also the correct bit
    out += 128*(2*c_comp_eq(32) + c_lin_gate());
    // 2.2.1 negate result
    out += 128*c_mux(1);

    // 3. reveal block
    out += 2*c_transmit(128);

    // 4. import local block into SC
    out += c_yaoShare(1, 128);

    // 5. blockmultiple times
    // 5.1 PRG
    out += blockMul*c_SC_PRG();
    // 6. copy block
    return out;
}

outType& FloramOrig::c_read() {
    // 1. calculate index of block and index within block
    outType& out = (b == 32)? 2*2848*c_lin_gate() : 2*4096*c_lin_gate();
    // 2. copy data of pass (126 GA) - for free in unconditional use-case

    // FSS-Gen
    // 3. fgbc: fss get blockvector with callback (25328 GA) + division
    out += fss_getblockvector_with_callback(true) + 2220*c_lin_gate();

    // 4. if data not found
    // 4.1 scanrom_read_with_blockvector
    out = out.addWR(scanrom_read_with_blockvector());
    // 4.2 set stashi[0] to -1

    // 6. for each element in the block
    // 6.1 check if this is the right subblock (64 GA)
    // 6.2 copy element from stash to output
    out += c*(c_comp_eq(64) + c_mux(32));
    return out;
}

// floram_apply
outType& FloramOrig::c_acc(uint64_t b) {
    // 1. calculate index of block and index within block
    outType& out = (b == 32)? 2*3968*c_lin_gate() : 2*4096*c_lin_gate();

    // 2. copy data of pass (126 GA)
    out += (128-myLog2(c))*c_lin_gate();

    // FSS-Gen
    // 3. fgbc: fss get blockvector with callback (25328 GA)
    out += fss_getblockvector_with_callback(false);

    // 4. if data not found
    // 4.1 scanrom_read_with_bitvector (10368 GA)
    // 4.2 setze stashi[0] auf blockid (32 GA)
    out = out.addWR(scanrom_read_with_bitvector() + c_mux(32));

    // 5. copy block from stash to temp block (128 GA)
    out += c_mux(bb);

    // 6. for each element in the block
    // 6.1 check if this is the right subblock (64 GA)
    // 6.2 fn (32 GA)
    out += c*(64*c_lin_gate() + c_mux(32));

    // 7. scanwrom write with blockvector (128 GA)
    out = out.addWR(scanwrom_write_with_blockvector());
    t++;
    return out;
}

outType& FloramOrig::c_acc(uint64_t noAcc, uint64_t b) {
    outType& mod = 0*c_lin_gate();
    outType& full = 0*c_lin_gate();

    for(uint64_t i = 1; i <= noAcc && i <= T; i++) {
        full = full.addWR(c_acc(b));

        if(i == noAcc % T)
            mod = (outType) {full.gates, full.traffic, full.rounds};
    }

    outType& epochs = multiplyWR((uint64_t) floor(noAcc / T), addWR(full, c_refresh()));
    // reset t
    t = noAcc % T;
    return addWR(epochs, mod);
}

outType& FloramOrig::c_refresh() {
    // 1. set progress to zero
    t = 0;
    // 2. get random bytes of KEYSIZE
    // 3. set new key of scanrom
    // 3.1 feed half key of party 1
    // 3.2 feed half key of party 2
    outType& out = 8*c_transmit(1280+3840);             // TODO !!!
    // 3.3 expand both keys
    out += 2560*c_lin_gate();
    // 4. import data from scanwrom
    out += 2*c_transmit(m*b);
    // 5. for each slot of stash till period: set used bit to false

    return out;
}

outType& FloramOrig::c_amortized(uint64_t noAcc, bool values) {

}

outType& FloramOrig::scanwrom_read() {
    // read yaoshare and import into circuit
    return c_yaoShare(blockMul, 128);
}

outType& FloramOrig::c_read_static() {
    return scanwrom_read();
}

outType& FloramOrig::c_write_static() {
    // 1. read block from OWOM
    // 2. update element in stash (if existent)
    // 3. write data to scanwrom (Y2B for free)
    // 3. import block from OWOM to OROM
    auto bw = (uint8_t) (m > 1024? 64 : 30);
    return scanwrom_read() + t*(c_comp_eq(bw) + c_mux((uint64_t) blockMul*128)) + 2*c_transmit((uint64_t) blockMul*128);
}

outType& FloramCPRGOrig::fss_getblockvector_with_callback(bool readOnly) {
    outType& out = fss_traverselevels(readOnly);
    return out;
}

// both parties had to perform eval(k^FSS, x) for all x to calculate z^(j, 0) and z^(j, 1)
// eval is evaluated simultaneously with gen by the parties
outType& FloramCPRGOrig::fss_traverselevels(bool readOnly) {
    auto blocks = (uint64_t) (readOnly? (numB+128-1)/128 : numB);
    // return pair of key values k_a^FSS and k_b^FSS (over course of function)
    // 1. let both parties compute z^(j, 0) and z⁽j, 1) locally (accumulation of left / right children)

    // 2. for each level of the tree
    // 2.1 compute relevant index of this level -> advice bit alpha_j
    // 2.2 submit accelerator_L and accelerator_R to SC (ocFromSharedN)
    outType& out = c_yaoShare(2, 128);

    // 3. select correct sum Z from acc_L and acc_R by using the advice bit alpha_j
    out += 2*c_mux(128);

    // 4. locally compute next advice words (sigma^j, tau^(j, 0), tau^(j, 1))
    // 5. reveal advice words L and R to both parties                                                                   // TODO: wieso partei 2 reveal Kosten?
    outType& out2 = addWR(out, 2*(2*c_transmit(8) + c_transmit(128)));

    // 6. return rest of keys that have not been revealed before TODO: ???
    return addWR(multiplyWR(clog2(blocks), out2), c_yaoShare(blockMul, 128));
}
