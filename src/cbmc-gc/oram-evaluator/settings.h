//
// Created by weber on 04.07.2017.
//

#ifndef ORAMEVALUATOR_SETTINGS_H_H
#define ORAMEVALUATOR_SETTINGS_H_H

#include "types/evalParam.h"

enum EvalTarget{TIME, BANDWIDTH, CIRCUIT_SIZE, ROUNDS};

class Settings {
protected:
    double_t latency{};                       // latency in ms
    double_t bandwidth{};                     // bandwidth in MBit/s
    double_t calc{};                          // calculation time in AND/ms

    uint16_t securityParam = 80;               // corresponds to security parameter 80
    uint16_t yaoSecurityParam = 128;           // corresponds to 256 bit per nonlinear gate

    EvalTarget evalTarget = TIME;
private:
    Settings() = default;
public:
    static Settings& get() {
        static Settings instance;
        return instance;
    }

    inline void set(double_t latency, double_t bandwidth, double_t calc, uint16_t sp) {
        get().latency = latency;
        get().bandwidth = bandwidth;
        get().calc = calc;
        get().securityParam = sp;
    }

    inline void setYao(uint16_t sp) {
        get().yaoSecurityParam = sp;
    }

    inline void setEvalTarget(EvalTarget target) {
        evalTarget = target;
    }

    double_t getLatency()const { return get().latency; }
    double_t getCalc() const { return get().calc; }
    double_t getBandwidth() const { return get().bandwidth; }
    uint16_t getSecurity() const { return get().securityParam; }
    uint16_t getYaoSecurity() const { return get().yaoSecurityParam; }
    EvalTarget getEvalTarget() const { return get().evalTarget; }

    Settings(const Settings&) = delete;
    Settings& operator=(const Settings&) = delete;
};

inline std::ostream& operator<<(std::ostream& ostr, const Settings& i) {
    return ostr << "___________________\nEvaluation Settings: \nLatency: " << i.getLatency() << " ms\nBandwidth: " << i.getBandwidth()
                << " MBit/s\nCalculation Time: " << i.getCalc() << " AND/ms\n\nSecurity Parameter: " << i.getSecurity()
                << " \nYao Security: " << i.getYaoSecurity() << "\n___________________";
}

// standard evaluation range for Binary Tree bucket size B
struct StandardBTB : evalParam {
    StandardBTB(uint16_t d) : evalParam((uint16_t) ((1+Settings::get().getSecurity()/20)*d),
                                        (uint16_t) ((5+Settings::get().getSecurity()/20)*d), 1, d) { }
};

// standard evaluation range for Path ORAM bucket size B
struct StandardPathB : evalParam {
    StandardPathB(uint16_t d) : evalParam(4, (uint16_t)(4 + d/2), 1, 2) { }
};

// standard evaluation range for Path-SC ORAM bucket size B
struct StandardPathSCB : evalParam {
    explicit StandardPathSCB(uint16_t d) : evalParam(4, (uint16_t)(4 + d/2), 2, 0) { }
};

// standard evaluation range for SCORAM bucket size B
struct StandardScoramB : evalParam {
    explicit StandardScoramB(uint16_t d) : evalParam(6, (uint16_t)(6 + d/2), 1, 2) { }
};

// standard evaluation range for Path ORAM bucket size B
struct StandardCoramB : evalParam {
    explicit StandardCoramB(uint16_t d) : evalParam(3, (uint16_t)(3 + d/2), 1, 2) { }
};

struct StandardPathS : evalParam {
    StandardPathS() : evalParam(
        (uint16_t) (ceil(1.2*Settings::get().getSecurity())-7),
        (uint16_t) (ceil(1.2*Settings::get().getSecurity())-2),
        1, 1) { }
};

struct StandardCoramS : evalParam {
    StandardCoramS() : evalParam(
            (uint16_t) (33),
            (uint16_t) (40),
            1, 1) { }
};

static constexpr uint16_t ScoramS[8] = {4, 5, 8, 13, 18, 23, 34, 50};

struct StandardScoramS : evalParam {
    explicit StandardScoramS(uint16_t d) : evalParam(
        (d >= 10 && d <= 24)?
            (uint16_t)(Settings::get().getSecurity() - 15 + ScoramS[(((d % 2 == 1)? d+1 : d) - 10) / 2]) :
            (uint16_t)(Settings::get().getSecurity() + 0.32589*pow(d, 2) - 8.7411*d + 40),
        (d >= 10 && d <= 24)?
            (uint16_t)(Settings::get().getSecurity() - 15 + ScoramS[(((d % 2 == 1)? d+1 : d) - 9) / 2]) :
            (uint16_t)(Settings::get().getSecurity() + 0.32589*pow(d, 2) - 8.7411*(d) + 50),
        1, 1) {  }
};

struct StandardSqrT : evalParam {
    explicit StandardSqrT(uint16_t d) : evalParam(
            1,
            (uint16_t) 2*ceil(sqrt(pow(2, d) * d - pow(2, d) + 1)),
            1, 1) { }
};

#endif //ORAMEVALUATOR_SETTINGS_H_H
