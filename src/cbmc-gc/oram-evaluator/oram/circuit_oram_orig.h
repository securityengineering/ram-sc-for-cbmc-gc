//
// Created by alina on 09.03.18.
//

#ifndef ORAM_EVALUATOR_CIRCUIT_ORAM_ORIG_H
#define ORAM_EVALUATOR_CIRCUIT_ORAM_ORIG_H

#include <cstdint>
#include "../helper.h"
#include "../types/outType.h"


class CORAMOrigRek {
protected:
    uint64_t m;
    uint64_t b;
    uint16_t d;
    uint16_t B;
    uint16_t s;

public:
    CORAMOrigRek(uint64_t m, uint64_t b) : m(m), b(b), d(myLog2(m)), B(3), s(33) {
        //std::cout << "neuer CORAM: m: " << m << " b: " << b << " d: " << d << " B: " << B << " s: " << s << std::endl;
    }
    virtual outType& access();
    virtual outType& read_and_remove();
    virtual outType& add_and_evict();
    virtual outType& extract();
    virtual outType& pack();

protected:
    virtual outType& stash_add();
    virtual outType& prepare();
    virtual outType& prepare_deepest();
    virtual outType& prepare_target();
    virtual outType& evict_once();
};

class CORAMOptRek : public CORAMOrigRek {

public:
    CORAMOptRek(uint64_t m, uint64_t b) : CORAMOrigRek(m, b) {
        //std::cout << "neuer CORAM: m: " << m << " b: " << b << " d: " << d << " B: " << B << " s: " << s << std::endl;
    }
    outType& access() override;
    outType& read_and_remove() override;
    outType& add_and_evict() override;
    outType& extract() override;
    outType& pack() override;

protected:
    outType& stash_add() override;
    outType& prepare() override;
    outType& prepare_deepest() override;
    outType& prepare_target() override;
    outType& evict_once() override;
};

class CORAMOrig {
private:
    CORAMOrigRek* maps[10] = { };
    uint64_t m;
    uint16_t levels{};
    uint64_t baseM{};
    uint16_t baseB{};

public:
    CORAMOrig(uint64_t m, uint64_t b) : m(m) { build(b); }

    virtual outType& access();

    virtual inline void build(uint64_t b) {
        uint64_t newM = m >> 3;
        baseM = m;
        maps[0] = new CORAMOrigRek(m, b);
        uint16_t i = 1;
        while(baseM > pow(2, 8)) {
            maps[i++] = new CORAMOrigRek(newM, (uint64_t) myLog2(baseM)*8);
            baseM = newM;
            newM = baseM >> 3;
        }
        levels = i;
        baseB = myLog2(baseM);
    }

protected:
    virtual outType& lumu(uint16_t stage);
    virtual outType& ls_read(uint64_t m, uint64_t b);
    virtual outType& ls_write(uint64_t m, uint64_t b);
};

class CORAMOpt {
private:
    CORAMOptRek* maps[10] = { };
    uint64_t m;
    uint16_t levels{};
    uint64_t baseM{};
    uint16_t baseB{};

public:
    CORAMOpt(uint64_t m, uint64_t b) : m(m) { build(b); }

    outType& access();

    inline void build(uint64_t b) {
        uint64_t newM = m >> 3;
        baseM = m;
        maps[0] = new CORAMOptRek(m, b);
        uint16_t i = 1;
        while(baseM > pow(2, 8)) {
            maps[i++] = new CORAMOptRek(newM, (uint64_t) myLog2(baseM)*8);
            baseM = newM;
            newM = baseM >> 3;
        }
        levels = i;
        baseB = myLog2(baseM);
    }

protected:
    outType& lumu(uint16_t stage);
    outType& ls_read(uint64_t m, uint64_t b);
    outType& ls_write(uint64_t m, uint64_t b);
};


#endif //ORAM_EVALUATOR_CIRCUIT_ORAM_ORIG_H
