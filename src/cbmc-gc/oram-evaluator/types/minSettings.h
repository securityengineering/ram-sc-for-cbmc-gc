//
// Created by weber on 14.10.2017.
//

#ifndef ORAMEVALUATOR_MINSETTINGS_H
#define ORAMEVALUATOR_MINSETTINGS_H

#include <cstdint>
#include "outType.h"

enum TYPE_ID_T {LS_ID, C_ORAM_ID, SQ_ORAM_ID, CPRG_ID, FLORAM_ID, UNSPECIFIED};

struct minSettings {
    std::string type;
    uint16_t type_id;
    uint16_t c{UINT16_MAX};
    uint16_t count{UINT16_MAX};
    outType* out{};

    minSettings() : out(new outType) { }
    minSettings(std::string type, uint16_t type_id, uint16_t c, uint16_t count, outType* out) :
            type(type), type_id(type_id), c(c), count(count), out(out) { }
};


inline std::ostream& operator<<(std::ostream& ostr, const minSettings& s) {
    return ostr << "type: " << s.type << " c: " << s.c << " count: " << s.count << " out: " << s.out;
}

struct btSettings : public minSettings {
    uint16_t B{UINT16_MAX};
    btSettings() = default;
    btSettings(std::string type, uint16_t B, uint16_t c, uint16_t count, outType* out) :
            minSettings(type,  C_ORAM_ID, c, count, out), B(B) { }
};


inline std::ostream& operator<<(std::ostream& ostr, const btSettings& s) {
    return ostr << "type: " << s.type << " B: " << s.B << " c: " << s.c << " count: " << s.count;
}

struct pathSettings : public btSettings {
    uint16_t stash{UINT16_MAX};
    pathSettings() = default;
    pathSettings(std::string type, uint16_t B, uint16_t c, uint16_t count, uint16_t stash, outType* out) :
            btSettings(type, B, c, count, out), stash(stash) { }
};

inline std::ostream& operator<<(std::ostream& ostr, const pathSettings& s) {
    return ostr << "type: " << s.type << " B: " << s.B << " stash: " << s.stash << " c: " << s.c << " count: " << s.count;
}

struct sqrtSettings : public minSettings {
    uint16_t T{UINT16_MAX};
    sqrtSettings() = default;
    sqrtSettings(std::string type, uint16_t T, uint16_t c, uint16_t count, outType* out) :
            minSettings(type, SQ_ORAM_ID, c, count, out), T(T) { }
};

inline std::ostream& operator<<(std::ostream& ostr, const sqrtSettings& s) {
    return ostr << "type: " << s.type << " T: " << s.T << " c: " << s.c << " count: " << s.count;
}

#endif //ORAMEVALUATOR_MINSETTINGS_H
