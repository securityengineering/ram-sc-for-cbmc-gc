#ifndef CPROVER_BMC_GC_PARSE_OPTIONS_H
#define CPROVER_BMC_GC_PARSE_OPTIONS_H

#include <cbmc/bmc.h>
#include "oram_goto_symext.h"

class bmc_gct : public bmct
{
private:
    cbmc_ramsc_goto_symext ramsc_symex;     // TODO: nicht wirklich stabil, später ändern
public:
  bmc_gct(const optionst &_options, const symbol_tablet &_symbol_table, message_handlert &_message_handler, prop_convt& _prop_conv, oramMapping& oramMap)
          : bmct(_options, _symbol_table, _message_handler, _prop_conv), ramsc_symex(bmct::symex, oramMap) {
  }
  
  virtual void setup_unwind() override {
      print_step_4("4.1.2: bmc_gct::setup_unwind()");
      bmct::setup_unwind(); }
  symex_target_equationt& symex_equation() { return equation; }
  //symex_bmct& symex() { return bmct::symex; }

    symex_bmct& symex() { return ramsc_symex; }
};

#endif
